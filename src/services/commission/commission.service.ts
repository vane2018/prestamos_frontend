import axios from 'axios';
import { APIs } from '../../constants/Constants';
import { ICommission } from './commission.interface';

export class CommissionService {   

    async findAll(): Promise<ICommission[]> {
        const token = localStorage.getItem('token');
        const rawResponse = await axios.get(APIs.COMMISSION,
            { headers: { Authorization: `Bearer ${token}` } });
        console.log('rawResponse.data ALL', rawResponse.data);
        return rawResponse.data;
    }

    async findOneByIdParam(id: string): Promise<ICommission> {
        const token = localStorage.getItem('token');
        const rawResponse = await axios.get(APIs.COMMISSION + '/' + id,
        { headers: { Authorization: `Bearer ${token}` } });
        console.log('rawResponse.data ONE ID', rawResponse.data);
        return rawResponse.data;
    }

    
}