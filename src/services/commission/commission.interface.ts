import { IUser } from "../users/user.interface";

export interface ICommission {

    readonly id: string;
    readonly commission: number;
    readonly description: string[];
    readonly typeReferido: string;
    readonly createAt: string;
    readonly user: IUser;

}