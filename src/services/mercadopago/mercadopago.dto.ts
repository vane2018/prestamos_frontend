export class MercadopagoDto {
    private transactionAmount:	string;
    private email:	string;
    private commerceId:	string;
    private shippingAmount:	string;   
    constructor(
        $transactionAmount: string,
        $email: string,
        $commerceId: string,
        $shippingAmount: string,        
    ) {

        this.transactionAmount = $transactionAmount;
        this.email = $email;
        this.commerceId = $commerceId;
        this.shippingAmount = $shippingAmount;
    }
     /**
    * Getter and Setter $transactionAmount
    */
    public get $transactionAmount(): string {
        return this.transactionAmount;
    }
    public set $transactionAmount(value: string) {
        this.transactionAmount = value;
    }
    /**
     * Getter and Setter $email
     */
    public get $email(): string {
        return this.email;
    }
    public set $email(value: string) {
        this.email = value;
    }
    /**
     * Getter and Setter $commerceId
     */
    public get $commerceId(): string {
        return this.commerceId;
    }
    public set $commerceId(value: string) {
        this.commerceId = value;
    }
    /**
     * Getter and Setter $shippingAmount
     */
    public get $shippingAmount(): string {
        return this.shippingAmount;
    }
    public set $shippingAmount(value: string) {
        this.shippingAmount = value;
    }
    
}