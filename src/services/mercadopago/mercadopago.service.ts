import axios from 'axios';
import { APIs } from '../../constants/Constants';
import { IMercadopago } from './mercadopago.interface';

export class MercadopagoService {
    async findAll(): Promise<IMercadopago[]> {
        const token = localStorage.getItem('token');
        console.log(token)
        const rawResponse = await axios.get(APIs.PAYMENTMERCADOPAGO,
            { headers: { Authorization: `Bearer ${token}` } });
        console.log('rawResponse.data MERCADOPAGO ALL', rawResponse.data);
        return rawResponse.data;
    }
}