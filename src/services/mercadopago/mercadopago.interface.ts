export interface IMercadopago {
    readonly id: string;
    readonly email: string;
    readonly transaction_amount: number;
    readonly status: string;
    readonly application_fee: number;
    readonly fee_details: {};
}