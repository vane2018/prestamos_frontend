import { ICommerce } from "../commerces/commerce.interface";

export interface IProduct {
    
    readonly id: string;
    readonly name : string;
    readonly description: string;
    readonly price: number;
    readonly pic: string[];
    readonly commerce: ICommerce;
}