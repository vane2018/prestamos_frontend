import axios from 'axios';
import { APIs } from '../../constants/Constants';
import { IProduct } from './product.interface';

export class ProductService {

    async findAll(): Promise<IProduct> {
        const token = localStorage.getItem('token');
        const rawResponse = await axios.get(APIs.PRODUCTS,
            { headers: { Authorization: `Bearer ${token}` } });
        console.log('rawResponse.data ALL', rawResponse.data);
        return rawResponse.data;
    }

    async findOneByIdParam(id: string): Promise<IProduct> {
        const token = localStorage.getItem('token');
        const rawResponse = await axios.get(APIs.PRODUCTS + '/' + id,
            { headers: { Authorization: `Bearer ${token}` } });
        console.log('rawResponse.data ONE ID', rawResponse.data);
        return rawResponse.data;
    }


}