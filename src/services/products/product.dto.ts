export class ProductDto {
    private name : string;
    private description: string;
    private price: number;
    private pic: string[];

    constructor(
        $name: string,
        $description: string,
        $price: number,
        $pic: string[],
        
    ) {

        this.name = $name;
        this.description = $description;
        this.price = $price;
        this.pic = $pic;


    }

     /**
    * Getter and Setter $name
    */
    public get $name(): string {
        return this.name;
    }
    public set $name(value: string) {
        this.name = value;
    }

    /**
     * Getter and Setter $description
     */
    public get $description(): string {
        return this.description;
    }
    public set $description(value: string) {
        this.description = value;
    }

    /**
     * Getter and Setter $price
     */
    public get $price(): number {
        return this.price;
    }
    public set $price(value: number) {
        this.price = value;
    }

    /**
     * Getter and Setter $pic
     */
    public get $pic(): string[] {
        return this.pic;
    }
    public set $pic(value: string[]) {
        this.pic = value;
    }
    
}