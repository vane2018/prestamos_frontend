export class UserDto {

    private password: string;
    private email: string;
    private firstName: string;
    private lastName: string;
    private nickReferrer: string;

    private phone: string;
    private gender: string;
    private dni: string;
    private barrio: string;
    private calle: string;
    private numero: string;
private localidad: string;
    private profilePic: string;
    private isCustomerAndSeller: boolean;
    private commisions: [];
    private commerce: {};
    private rols: {};


    constructor(
        $password: string,
        $email: string,
        $firstName: string,
        $lastName: string,
        $nickReferrer: string,
$localidad: string,
        $phone: string,
        $gender: string,
        $dni: string,
        $profilePic: string,
        $barrio: string,
        $calle: string,
        $numero: string,
        $isCustomerAndSeller: boolean,
        $commisions: [],
        $commerce: {},
        $rols: {},
    ) {

        this.password = $password;
        this.email = $email;
        this.firstName = $firstName;
        this.lastName = $lastName;
        this.nickReferrer = $nickReferrer;
        this.localidad=$localidad;

        this.phone = $phone;
        this.gender = $gender;
        this.dni = $dni;
        this.profilePic = $profilePic;
        this.barrio = $barrio;
        this.calle= $calle;
        this.numero= $numero;
        this.isCustomerAndSeller = $isCustomerAndSeller;
        this.commisions = $commisions;
        this.commerce = $commerce;
        this.rols = $rols;

    }

     /**
    * Getter and Setter $password
    */
    public get $password(): string {
        return this.password;
    }
    public set $password(value: string) {
        this.password = value;
    }

    /**
     * Getter and Setter $email
     */
    public get $email(): string {
        return this.email;
    }
    public set $email(value: string) {
        this.email = value;
    }

    /**
     * Getter and Setter $firstName
     */
    public get $firstName(): string {
        return this.firstName;
    }
    public set $firstName(value: string) {
        this.firstName = value;
    }

    /**
     * Getter and Setter $lastName
     */
    public get $lastName(): string {
        return this.lastName;
    }
    public set $lastName(value: string) {
        this.lastName = value;
    }

    /**
     * Getter and Setter $nickReferrer
     */
    public get $nickReferrer(): string {
        return this.nickReferrer;
    }
    public set $nickReferrer(value: string) {
        this.nickReferrer = value;
    }

    public get $localidad(): string {
        return this.localidad;
    }
    public set $localidad(value: string) {
        this.localidad = value;
    }

    

    /**
     * Getter and Setter $phone
     */
    public get $phone(): string {
        return this.phone;
    }
    public set $phone(value: string) {
        this.phone = value;
    }

    
    /**
     * Getter and Setter $gender
     */
    public get $gender(): string {
        return this.gender;
    }
    public set $gender(value: string) {
        this.gender = value;
    }

    /**
     * Getter and Setter $dni
     */
    public get $dni(): string {
        return this.dni;
    }
    public set $dni(value: string) {
        this.dni = value;
    }

    /**
     * Getter and Setter $profilePic
     */
    public get $profilePic(): string {
        return this.profilePic;
    }
    public set $profilePic(value: string) {
        this.profilePic = value;
    }

    public get $calle(): string {
        return this.$calle;
    }
    public set $calle(value: string) {
        this.calle = value;
    }

    public get $barrio(): string {
        return this.$barrio;
    }
    public set $barrio(value: string) {
        this.barrio = value;
    }

    public get $numero(): string {
        return this.$numero;
    }
    public set $numero(value: string) {
        this.numero = value;
    }

    /**
     * Getter and Setter $isCustomerAndSeller
     */
    public get $isCustomerAndSeller(): boolean {
        return this.isCustomerAndSeller;
    }
    public set $isCustomerAndSeller(value: boolean) {
        this.isCustomerAndSeller = value;
    }

    /**
     * Getter and Setter $commisions
     */
    public get $commisions(): [] {
        return this.commisions;
    }
    public set $commisions(value: []) {
        this.commisions = value;
    }

    /**
     * Getter and Setter $commerce
     */
    public get $commerce(): {} {
        return this.commerce;
    }
    public set $commerce(value: {}) {
        this.commerce = value;
    }

    /**
     * Getter and Setter $rols
     */
    public get $rols(): {} {
        return this.rols;
    }
    public set $rols(value: {}) {
        this.rols = value;
    }

}