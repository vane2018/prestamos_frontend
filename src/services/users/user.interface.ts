export interface IRole {

  readonly id: number;

  readonly name: string;
  
  readonly description: string;

}

export interface IUser {

  readonly id: string;

  readonly password: string;

  readonly email: string;

  readonly firstName: string;

  readonly lastName: string;

  readonly nickReferrer: string;

  readonly nickName: string;

  readonly phone: string;

  readonly gender: string;

  readonly dni: string;
  
  readonly cbu: string;

  readonly lastAccess: string;
  
  readonly barrio: string;

  readonly calle: string;

  readonly numero: string;
  readonly localidad:string;


  readonly profilePic: string;

  readonly isCustomerAndSeller: string;

  readonly commisions: any[];

  readonly rols: IRole[];
}