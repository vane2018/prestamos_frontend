import axios from 'axios';
import { APIs } from '../../constants/Constants';
import { UserEditDto } from './user.edit.dto';
import { IUser } from './user.interface';

export class UserService {

    async findAll(queryString: string): Promise<IUser> {
        const token = localStorage.getItem('token');
        const rawResponse = await axios.get(APIs.USERS + '/?' + queryString,
            { headers: { Authorization: `Bearer ${token}` }});
        console.log('rawResponse.data ALL', rawResponse.data);
        return rawResponse.data;
    }

    async findOneByIdParam(id: string): Promise<IUser> {
       
   
        console.log("ESTOY BUSCANDO A",id);
        
        const token = localStorage.getItem('token');
        const rawResponse = await axios.get(APIs.USERS + '/' + id,
        { headers: { Authorization: `Bearer ${token}` }});
        console.log('rawResponse.data ONE ID', rawResponse.data);
        return rawResponse.data;
    }
    async findOneByIdParam2(id: string): Promise<IUser> {
     console.log("ingreso a buscar");
     
        const rawResponse = await axios.get(APIs.USERS + '/' + id
        );
        console.log('rawResponse.data ONE ID', rawResponse.data);
        return rawResponse.data;
    }

    async editarUser(id: string, user: UserEditDto) {
        let miId=id.slice(1);
        miId=miId.slice(0,-1);
  
        
       console.log("ingreso a editar front",user)
        const rawResponse = await axios.post(APIs.USERSEDIT + '/' + id,user);
        console.log('rawResponse.data ONE ID', rawResponse.data);
        return rawResponse.data;
    }

    async updateUser(userID: string, data: any): Promise<any> {
        let miId=userID.slice(1);
        miId=miId.slice(0,-1);
        console.log("por que funciona",miId);
        console.log("verifico los nuevos datos",data);
        
        
        const user = await this.findOneByIdParam(miId);
        console.log('MOSTRAMOS EL USER PARA UPDATE', user);
        if (!user) {
          console.log('user not found');
        }
        console.log("ruta",APIs.USERSEDIT + '/' + miId);
        data.id=miId;
        data.password='123'
        console.log("DATA ENTRANTE",data);
        
        const updated = await axios.put(APIs.USERSEDIT + '/' + miId, data);
        console.log('updatedUser', updated);
        return updated.data;
      }

      async removeUser(id: string) {
    
   
        console.log("ESTOY BUSCANDO A",id);
        
        
        const rawResponse = await axios.delete(APIs.USERS + '/' + id,
      );
        console.log('rawResponse.data ONE ID', rawResponse.data);
        return rawResponse.data;
    }

}
