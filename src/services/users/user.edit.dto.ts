export class UserEditDto {


    private firstName: string;
    private lastName: string;
    private dni: string;
    private localidad: string;
    private provincia: string;
    private pais: string;
    private phone: string;
    private barrio: string;
    private gender: string;
    private calle: string;
   
    private numero: string;
   

    constructor(
      
        $firstName: string,
        $lastName: string,
        $dni: string,
        $localidad: string,
        
        $provincia: string,
        $pais: string,
    
        $phone: string,
        $gender: string,
        $barrio: string,
        $calle: string,
        $numero: string
      
    ) {
       
      
        this.firstName = $firstName;
        this.lastName = $lastName;
        this.dni = $dni;
        this.localidad = $localidad;
        this.provincia = $provincia;
        this.pais = $pais;
       
        this.phone = $phone;
        this.gender = $gender;
       
        this.barrio= $barrio;
        this.calle= $calle;
        this.numero= $numero;

    }

 

    /**
     * Getter and Setter $email
     */
    

    public get $dni(): string {
        return this.dni;
    }
    public set $dni(value: string) {
        this.dni = value;
    }

    public get $localidad(): string {
        return this.localidad;
    }
    public set $localidad(value: string) {
        this.localidad = value;
    }
     /*  Getter and Setter $firstName */
     
    public get $firstName(): string {
        return this.firstName;
    }
    public set $firstName(value: string) {
        this.firstName = value;
    }

    /**
     * Getter and Setter $lastName
     */
    public get $lastName(): string {
        return this.lastName;
    }
    public set $lastName(value: string) {
        this.lastName = value;
    }


  

    /**
     * Getter and Setter $phone
     */
    public get $phone(): string {
        return this.phone;
    }
    public set $phone(value: string) {
        this.phone = value;
    }

    
    /**
     * Getter and Setter $gender
     */
    public get $gender(): string {
        return this.gender;
    }
    public set $gender(value: string) {
        this.gender = value;
    }


    /**
     * Getter and Setter $calle
     */
     public get $calle(): string {
        return this.calle;
    }
    public set $calle(value: string) {
        this.calle = value;
    }

    /**
     * Getter and Setter $barrio
     */
     public get $barrio(): string {
        return this.barrio;
    }
    public set $barrio(value: string) {
        this.barrio = value;
    }

    /**
     * Getter and Setter $numero
     */
     public get $numero(): string {
        return this.numero;
    }
    public set $numero(value: string) {
        this.numero = value;
    }

    

    public get $pais(): string {
        return this.pais;
    }
    public set $pais(value: string) {
        this.pais = value;
    }

    public get $provincia(): string {
        return this.provincia;
    }
    public set $provincia(value: string) {
        this.provincia = value;
    }


}