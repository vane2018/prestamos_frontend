import axios from 'axios';
import { APIs } from '../../constants/Constants';
import { IUser } from '../users/user.interface';
import { UserRegisterDto } from '../users/user.register.dto';
import { LoanRegisterDto } from './loan.register.dto';
import { ILoan } from './loan.interface';


export class LoanService {

 /*  async loginUser(loginUser: LoanRegisterDto): Promise<any> {
    
    const rawResponse = await axios.post(APIs.LOGIN, loginUser);
    return rawResponse.data;
  }
 */
  async createLoan(userData: LoanRegisterDto): Promise<ILoan> {

    const rawResponse = await axios.post(APIs.CREATELOAN, userData);
    console.log("loan creado2",rawResponse)
    return rawResponse.data;
  }
 /*  async findAll(): Promise<ILoan> {
    const token = localStorage.getItem('token');
    const rawResponse = await axios.get(APIs.USERS,
        { headers: { Authorization: `Bearer ${token}` } });
    console.log('rawResponse.data ALL', rawResponse.data);
    return rawResponse.data;
} */
async findAll(queryString: string): Promise<ILoan> {
  const token = localStorage.getItem('token');
  const rawResponse = await axios.get(APIs.LOANS + '/?' + queryString,
      { headers: { Authorization: `Bearer ${token}` }});
  console.log('rawResponse.data ALL', rawResponse.data);
  return rawResponse.data;
}

async findOneByIdParam(id: string): Promise<ILoan> {
    
   
  console.log("ESTOY BUSCANDO A",id);
  console.log("ruta edit",APIs.LOANS + '/' + id);
  
  const token = localStorage.getItem('token');
  const rawResponse = await axios.get(APIs.LOANS + '/' + id,
  { headers: { Authorization: `Bearer ${token}` }});
  console.log('rawResponse.data ONE ID', rawResponse.data);
  return rawResponse.data;
}


async updateLoan(userID: string, data: any): Promise<any> {
  console.log("verifico update", userID)
  console.log("verifico update", data)
  let miId=userID.slice(1);
  miId=miId.slice(0,-1);
  console.log("por que funciona",miId);
  console.log("verifico los nuevos datos",data);
  
  
  const user = await this.findOneByIdParam(userID);
  console.log('MOSTRAMOS EL USER PARA UPDATE', user);
  if (!user) {
    console.log('user not found');
  }
  console.log("ruta",APIs.LOANSEDIT + '/' + userID);
data.user=data.user.id;
data.interes= parseInt(data.interes);
data.monto=parseInt(data.monto);
data.montoTotal=parseInt(data.montoTotal);
console.log("DATA QUE ENVIO A POST",data);

  const updated = await axios.put(APIs.LOANSEDIT + '/' + userID, data);
  console.log('updatedUser', updated);
  return updated.data;
}

async removeLoans(id: string) {
  console.log("loans a eliminar",id);
  


  console.log("ESTOY BUSCANDO A",id);
  
  
  const rawResponse = await axios.delete(APIs.LOANS + '/' + id,
);
  console.log('rawResponse.data ONE ID', rawResponse.data);
  return rawResponse.data;
}
}