import { UserDto } from '../users/user.dto';
import { IUser } from '../users/user.interface';

  
  export interface ILoan {
    readonly id: string;

    readonly date: Date;
  
    readonly monto: number;
  
    readonly interes: number;
  
    readonly montoTotal: number;
  
    readonly prenda: string;
  
    readonly origen: string;
  
    readonly descripcion: string;

  
    readonly user: IUser;
  
  }