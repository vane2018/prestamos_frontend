
export class LoanRegisterDto {

    private date: Date;
    private monto: number;
    private interes: number;
    private montoTotal: number;
    private user: string;
   
    private prenda: string;
    private origen: string;
    private descripcion: string;
    


    constructor(
        $date:Date,
        $monto: number,
        $interes: number,
        $montoTotal: number,
    $user:string,
        $prenda: string,
        $origen: string,
        $descripcion: string,
       
      
    ) {
        this.date = $date;
        this.monto = $monto;
        this.interes= $interes;
        this.montoTotal= $montoTotal;
        this.user=$user;
     
        this.prenda = $prenda;
        this.origen = $origen;
        this.descripcion= $descripcion;     
     

    }

    /**
    * Getter and Setter $password
    */
    public get $date(): Date{
        return this.date;
    }
    public set $date(value: Date) {
        this.date = value;
    }

    public get $monto(): number{
        return this.monto;
    }
    public set $monto(value: number) {
        this.monto = value;
    }

    /**
     * Getter and Setter $email
     */
    public get $interes(): number {
        return this.interes;
    }
    public set $interes(value: number) {
        this.interes = value;
    }

    /**
     * Getter and Setter $montoTotal
     */
    public get $montoTotal(): number {
        return this.montoTotal;
    }
    public set $montoTotal(value: number) {
        this.montoTotal = value;
    }
    public get $user(): string {
        return this.user;
    }
    public set $user(value: string) {
        this.user = value;
    }

    /**
     * Getter and Setter $lastName
     */
    public get $prenda(): string {
        return this.prenda;
    }
    public set $prenda(value: string) {
        this.prenda = value;
    }


  

    /**
     * Getter and Setter $phone
     */
    public get $origen(): string {
        return this.origen;
    }
    public set $origen(value: string) {
        this.origen = value;
    }

    
    /**
     * Getter and Setter $gender
     */
    public get $descripcion(): string {
        return this.descripcion;
    }
    public set $descripcion(value: string) {
        this.descripcion = value;
    }

    


}