export class LoginUserDto {
  private email: string;
  private nickName: string;
  private password: string;

  constructor($email: string, $nickName: string, $password: string) {
    this.email = $email;
    this.nickName = $nickName;
    this.password = $password;
  }

  /**
   * Getter $name
   * @return {string}
   */
  public get $email(): string {
    return this.email;
  }

  public get $nickName(): string {
    return this.nickName;
  }
  /**
   * Getter $adress
   * @return {string}
   */
  public get $password(): string {
    return this.password;
  }

  /**
   * Setter $name
   * @param {string} value
   */
  public set $email(value: string) {
    this.email = value;
  }

  public set $nickName(value: string) {
    this.nickName = value;
  }

  /**
   * Setter $adress
   * @param {string} value
   */
  public set $password(value: string) {
    this.password = value;
  }
}
