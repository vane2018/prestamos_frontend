import axios from 'axios';
import { APIs } from '../../constants/Constants';
import { LoginUserDto } from './login.dto';
import { IUser } from '../users/user.interface';
import { UserRegisterDto } from '../users/user.register.dto';


export class LoginService {

  async loginUser(loginUser: LoginUserDto): Promise<any> {
    
    const rawResponse = await axios.post(APIs.LOGIN, loginUser);
    return rawResponse.data;
  }

  async createUserAdmin(userData: UserRegisterDto): Promise<IUser> {

    const rawResponse = await axios.post(APIs.CREATEADMIN, userData);
    return rawResponse.data;
  }
  
  async createUser(userData: UserRegisterDto): Promise<IUser> {

    const rawResponse = await axios.post(APIs.CREATEUSER, userData);
    console.log(rawResponse);
    
    return rawResponse.data;
  }
}