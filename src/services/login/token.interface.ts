export interface IToken {
    readonly id: string;
    readonly email: string;
    readonly profilePic: string;

}