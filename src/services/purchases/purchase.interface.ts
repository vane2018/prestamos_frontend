export interface IPurchase {

    readonly id: number;
  
    readonly date: string;
    
    readonly createAt: string;
  
  }