import axios from 'axios';
import { APIs } from '../../constants/Constants';
import { IPurchase } from './purchase.interface';

export class PurchaseService {

    async findAll(): Promise<IPurchase> {
        const token = localStorage.getItem('token');
        const rawResponse = await axios.get(APIs.PURCHASES,
            { headers: { Authorization: `Bearer ${token}` } });
        console.log('rawResponse.data ALL', rawResponse.data);
        return rawResponse.data;
    }

    async findOneByIdParam(id: string): Promise<IPurchase> {
        const token = localStorage.getItem('token');
        const rawResponse = await axios.get(APIs.PURCHASES + '/' + id,
            { headers: { Authorization: `Bearer ${token}` } });
        console.log('rawResponse.data ONE ID', rawResponse.data);
        return rawResponse.data;
    }


}