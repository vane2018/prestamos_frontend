export class PurchaseDto {

    private date: string;
    private payment: string;
    private user: string;
    private products: string[];



    constructor(
        $date: string,
        $payment: string,
        $user: string,
        $products: string[],
    
    ) {

        this.date = $date;
        this.payment = $payment;
        this.user = $user;
        this.products = $products;  

    }

     /**
    * Getter and Setter $date
    */
    public get $date(): string {
        return this.date;
    }
    public set $date(value: string) {
        this.date = value;
    }

    /**
     * Getter and Setter $payment
     */
    public get $payment(): string {
        return this.payment;
    }
    public set $payment(value: string) {
        this.payment = value;
    }

    /**
     * Getter and Setter $user
     */
    public get $user(): string {
        return this.user;
    }
    public set $user(value: string) {
        this.user = value;
    }

    /**
     * Getter and Setter $products
     */
    public get $products(): string[] {
        return this.products;
    }
    public set $products(value: string[]) {
        this.products = value;
    }

    

}