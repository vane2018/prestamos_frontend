import { UserDto } from "../users/user.dto";

export class CommerceDto {

    private name: string;
    private adress: string;
    private cuit: number;
    private profilePic: string;
    private coverPic: string;
    private user: UserDto;

    constructor(
        $name: string,
        $adress: string,
        $cuit: number,
        $profilePic: string,
        $coverPic: string,
        $user: UserDto
    ) {
        this.name = $name
        this.adress = $adress;
        this.cuit = $cuit;
        this.profilePic = $profilePic;
        this.coverPic = $coverPic;
        this.user = $user;
    }

    /**
    * Getter and Setter $name
    */
    public get $name(): string {
        return this.name;
    }
    public set $name(value: string) {
        this.name = value;
    }

    /**
    * Getter and Setter $adress
    */
    public get $adress(): string {
        return this.adress;
    }
    public set $adress(value: string) {
        this.adress = value;
    }

    /**
    * Getter and Setter $cuit
    */
    public get $cuit(): number {
        return this.cuit;
    }
    public set $cuit(value: number) {
        this.cuit = value;
    }

    /**
    * Getter and Setter $profilePic
    */
    public get $profilePic(): string {
        return this.profilePic;
    }
    public set $profilePic(value: string) {
        this.profilePic = value;
    }

    /**
    * Getter and Setter $coverPic
    */
    public get $coverPic(): string {
        return this.coverPic;
    }
    public set $coverPic(value: string) {
        this.coverPic = value;
    }

    /**
    * Getter and Setter $user
    */
    public get $user(): UserDto {
        return this.user;
    }
    public set $user(value: UserDto) {
        this.user = value;
    }

}