import axios from 'axios';
import { APIs } from '../../constants/Constants';
import { ICommerce } from './commerce.interface';

export class CommerceService {

    async findAll(): Promise<ICommerce> {
        const token = localStorage.getItem('token');
        const rawResponse = await axios.get(APIs.COMMERCER,
            { headers: { Authorization: `Bearer ${token}` } });
        console.log('rawResponse.data ALL', rawResponse.data);
        return rawResponse.data;
    }

    async findOneByIdParam(id: string): Promise<ICommerce> {
        const token = localStorage.getItem('token');
        const rawResponse = await axios.get(APIs.COMMERCER + '/' + id,
            { headers: { Authorization: `Bearer ${token}` } });
        console.log('rawResponse.data ONE ID', rawResponse.data);
        return rawResponse.data;
    }


}