import { UserDto } from "../users/user.dto";
import { IUser } from "../users/user.interface";

export interface ICommerce {
    readonly id: string; 
    readonly name: string;
    readonly adress: string;
    readonly cuit: number;
    readonly profilePic: string;
    readonly coverPic: string;
    readonly user: IUser;
}