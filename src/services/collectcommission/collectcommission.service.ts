import axios from 'axios';
import { APIs } from '../../constants/Constants';
import { ICollectCommission } from './collectcommission.interface';

export class CollectCommissionService {

    async findAll(): Promise<ICollectCommission[]> {
        const token = localStorage.getItem('token');
        const rawResponse = await axios.get(APIs.COLLECTCOMMISSION,
            { headers: { Authorization: `Bearer ${token}` } });
        console.log('rawResponse.data ALL', rawResponse.data);
        return rawResponse.data;
    }
    async editStatusByIdParam(id: string, statusParam: any): Promise<ICollectCommission[]> {
        const rawResponse = await axios.put(APIs.COLLECTCOMMISSIONEDIT + '/' + id, statusParam);
        console.log('rawResponse.data ALL', rawResponse.data);
        return rawResponse.data;
    }
}