import { IUser } from "../users/user.interface";

export interface ICollectCommission {
    readonly id: string;
    readonly estado: string;
    readonly idusuario: string;
    readonly monto: number;
    readonly user: IUser;
}