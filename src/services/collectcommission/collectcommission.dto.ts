export class CollectCommissionDto {
    private commission: number;
    private description: string[];
    private typeReferido: string;
    private idUser: string;
    private createAt: Date;

    constructor(
        $commission: number,
        $description: string[],
        $typeReferido: string,
        $idUser: string,
        $createAt: Date,
    ) {

        this.commission = $commission;
        this.description = $description;
        this.typeReferido = $typeReferido;
        this.idUser = $idUser;
        this.createAt = $createAt;
    }

    /**
    * Getter and Setter $commission
    */
    public get $commission(): number {
        return this.commission;
    }
    public set $commission(value: number) {
        this.commission = value;
    }

    /**
     * Getter and Setter $typeReferido
     */
    public get $typeReferido(): string {
        return this.typeReferido;
    }
    public set $typeReferido(value: string) {
        this.typeReferido = value;
    }

    /**
     * Getter and Setter $description
     */
    public get $description(): string[] {
        return this.description;
    }
    public set $description(value: string[]) {
        this.description = value;
    }

    /**
     * Getter and Setter $idUser
     */
    public get $idUser(): string {
        return this.idUser;
    }
    public set $idUser(value: string) {
        this.idUser = value;
    }

    /**
     * Getter and Setter $createAt
     */
    public get $createAt(): Date {
        return this.createAt;
    }
    public set $createAt(value: Date) {
        this.createAt = value;
    }
}