export interface IMercadoPago {
  id: string;
  accessToken: string;
  tokenType: string;
  expireIn: number;
  scope: string;
  refreshToken: string;
  comercioId: string;
  deletedAt: Date;
  createdAt: Date;
}