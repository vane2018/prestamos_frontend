import { SET_CURRENT_USER } from '../actions/auth.action'
import isEmpty from '../validations/auth.validation'

export default function (state: any, action: any) {
    switch (action.type) {
        case SET_CURRENT_USER:
            return {
                ...state,
                isAuthenticated: !isEmpty(action.payload),
                user: action.payload
            }
        default:
            return state;
    }
}