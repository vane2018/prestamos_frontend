export const SET_CURRENT_USER = "SET_CURRENT_USER";


//si se loguea , setear datos del usuario
export const setCurrentUser = (decoded: any) => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};

//logout
export const logoutUser = (dispatch: any) => {
  localStorage.removeItem("token");
  dispatch(setCurrentUser({}));
};