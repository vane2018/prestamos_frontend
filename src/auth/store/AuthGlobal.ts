import React, { Dispatch } from "react";
interface IContextProps {
    stateUser: {
        isAuthenticated: null,
        user: {}
    }
    dispatch: Dispatch<any>;
}
export default React.createContext({} as IContextProps);