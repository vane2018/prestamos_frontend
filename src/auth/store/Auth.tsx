import React, { useReducer, useEffect, useState } from 'react';
import authReducer from '../reduces/auth.reduce';
import { setCurrentUser } from '../actions/auth.action';
import AuthGlobal from './AuthGlobal';
import jwt_decode from "jwt-decode";

const Auth = (props: { children: React.ReactNode; }) => {    
    const [stateUser, dispatch] = useReducer(authReducer, {
        isAuthenticated: null,
        user: {}
    });
    const [showChild, setShowChild] = useState(false);

    useEffect(() => {
        if (localStorage.token) {
            const decoded = localStorage.token ? localStorage.token : "";
            dispatch(setCurrentUser(jwt_decode(decoded))); 
        }
        setShowChild(true);
    }, []); 

    if (!showChild) {
        return null;
    } else {
        return (
            <AuthGlobal.Provider
                value={{
                    stateUser,
                    dispatch
                }}
            >
                {props.children}
            </AuthGlobal.Provider>
        );
    }
};

export default Auth;