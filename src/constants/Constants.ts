const SERVER_URL =
    process.env.NODE_ENV === 'production'
        ? 'http://192.168.1.19:3000'
        : 'http://192.168.100.4:3000';
         //'http://192.168.1.15:3000 https://api.comprartir-staging.com.ar';

export const APIs = {
     
    COMMERCER: SERVER_URL + '/api/v1/commerces',
    COMMISSION: SERVER_URL + '/api/v1/commission',
    COLLECTCOMMISSION: SERVER_URL + '/api/v1/cobrarcomision/obtener/comision',
    COLLECTCOMMISSIONEDIT: SERVER_URL + '/api/v1/cobrarcomision/edit',
    CREATEADMIN: SERVER_URL + '/api/v1/auth/registerAdmin',
    CREATEUSER: SERVER_URL + '/api/v1/auth/register',
    LOGIN: SERVER_URL + '/api/v1/auth/login',    
    PRODUCTS: SERVER_URL + '/api/v1/products',
    LOANS: SERVER_URL + '/api/v1/loans',
    LOANSEDIT: SERVER_URL + '/api/v1/loans/editLoan',
    CREATELOAN: SERVER_URL + '/api/v1/loans/register',
    PURCHASES: SERVER_URL + '/api/v1/purchases',
    USERS: SERVER_URL + '/api/v1/users',
    USERSEDIT: SERVER_URL + '/api/v1/users',
    MERCADOPAGO: SERVER_URL + '/apis/v1/mercado-pago',
    PAYMENTMERCADOPAGO: SERVER_URL + '/api/v1/mercado/apis/v1/mercado-pago/gestion',
   
};