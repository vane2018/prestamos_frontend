import React from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import LayoutPage from './components/layout/LayoutPage';
import LayoutMercadoPago from './components/layout/LayoutMercadoPago';

import LoginPage from './components/login/LoginPage';
import RegisterAdmin from './components/login/registerAdmin/RegisterAdmin';
import MercadoPago from './components/mercadopago/MercadoPago';
import MercadopagoLoading from './components/mercadopago/MercadopagoLoading';
import Auth from './auth/store/Auth';
import './App.css';
import Register from './components/register/Register';


const App: React.FC = () => {
  
  return (
    <Auth>
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route path="/dashboar/miComercio/mercado-pago/:idParam" component={MercadoPago} />
            <Route exact path="/mercado-pago" component={LayoutMercadoPago} />
            <Route exact path="/mercado-pago/loading-sppiner" component={MercadopagoLoading} />
            <Route path="/login" component={LoginPage} />
            <Route path="/register-form" component={RegisterAdmin} />  
        
                
            <Route path="/" component={LayoutPage} />
          </Switch>
        </BrowserRouter>
      </div>
    </Auth>
  );
}

export default App;
