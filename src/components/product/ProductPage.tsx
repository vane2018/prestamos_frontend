import React, { useState, useEffect } from 'react';
import { Avatar, Typography } from 'antd';
import { List } from 'antd';
import { ProductService } from '../../services/products/product.service';
import { IProduct } from '../../services/products/product.interface';

const productService = new ProductService();

const ProductPage = () => {
    const [products, setProducts] = useState<IProduct[]>();

    const loadProduct = async () => {
        const res: any = await productService.findAll();
        setProducts(res);
    }

    useEffect(() => {
        loadProduct();

    }, []);
    return (
        <List
            grid={{
                gutter: 16,
                xs: 1,
                sm: 1,
                md: 2,
                lg: 3,
                xl: 3,
                xxl: 3,
            }}
            dataSource={products}
            renderItem={item => (
                <List.Item key={item.name}>
                    <List.Item.Meta
                        avatar={
                            <Avatar shape="square" src={item.pic[0]} />
                        }
                        title={<Typography.Text strong >{item.name}</Typography.Text>}
                        description={<Typography.Text>{'Precio: $' + item.price}</Typography.Text>}
                    />

                    <div style={{ marginLeft: 50 }}>
                        <span>
                            <Typography.Text >{'Comercio: ' + item.commerce.name}</Typography.Text>
                        </span>
                        <br/>
                        <span>
                            <Typography.Text >{'Dirección: ' + item.commerce.adress}</Typography.Text>
                        </span>
                        <br/>
                        <span>
                            <Typography.Text >{'Descripción: ' + item.description}</Typography.Text>
                        </span>
                    </div>
                </List.Item>
            )}

        />

    );
}
export default ProductPage;