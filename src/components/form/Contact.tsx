import React, { useState } from 'react';
import {
    Form,
    Input,
    Button,
    Select,
    InputNumber,
    Row,
    Col,
    Space,
} from 'antd';

const Contact = () => {

    const [labelName, setLabelName] = useState('Nombre y Apellido *');

    const onchangeEmail = (value: any) => {
        console.log('aque', value.target.value);
        setLabelName('otro valor');
    }


    return (
            <Form
                layout='vertical'
                initialValues={{ size: 'large' }}
                // onValuesChange={onFormLayoutChange}
                size='large'
            >
                <Row justify='space-between'>

                    <Form.Item
                        name={'Campo1'}
                        rules={[{ type: 'email' }]}
                    >
                        <Input
                            placeholder='Email:'
                            style={{ width: 280 }}
                            onChange={onchangeEmail}
                        />
                    </Form.Item>
                    <Form.Item
                        name={'Campo2'}
                    >
                        <Input
                            placeholder={labelName}
                            style={{ width: 280 }}
                        />
                    </Form.Item>
                    <Form.Item
                        name={'Campo3'}
                        rules={[{ type: 'number' }]}
                    >
                        <InputNumber
                            placeholder='Telefono'
                            style={{
                                width: 280,
                            }}
                        />
                    </Form.Item>
                </Row>
                <Row justify='space-between'>
                    <Form.Item name={['Campo4']} >
                        <Input.TextArea
                            placeholder='tu mensaje'
                            
                        />
                    </Form.Item>

                </Row>

                <Row style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Form.Item>
                        <Button
                            type="primary"
                            htmlType="submit"
                            style={{ width: 300 }}
                        >
                            Enviar
                    </Button>
                    </Form.Item>

                </Row>

            </Form>
    );
};
export default Contact;