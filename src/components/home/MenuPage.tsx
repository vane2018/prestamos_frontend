import React, { useEffect, useState, Suspense } from 'react';
import {
  Typography,
  Row,
  Col,
  Card,
  Statistic,
  Drawer,
  Tooltip,
  Button,
} from 'antd';
import {
  ArrowUpOutlined,
  ArrowDownOutlined,
  NotificationOutlined,
  MessageOutlined,
  EyeOutlined,
  InfoCircleOutlined
} from '@ant-design/icons/lib/icons';
import CommissionIcon from '../../resources/icons/commission-icon.svg'
import { ChartCard, Pie } from 'ant-design-pro/lib/Charts';
import { CommissionService } from '../../services/commission/commission.service';
import { IUser } from '../../services/users/user.interface';
import { ICommission } from '../../services/commission/commission.interface';
import MapComprartir from './component/MapComprartir'
import Profil from './component/Profile';
import 'ant-design-pro/dist/ant-design-pro.css';
import './MenuPage.css';


const { Title, Text } = Typography;

const commissionService = new CommissionService();

const MenuPage = (props: any) => {
  const [state, setState] = useState<IUser>();
  const [visible, setVisible] = useState(false);
  const [commissions, setCommissions] = useState<ICommission[]>();
  const [commmissionTotal, setCommissionTotal] = useState(0);
  const [commissionPieData, setCommissionPieData] = useState<any[]>();

  const showDrawer = () => {
    setVisible(!visible)
  }

  useEffect(() => {
    if (state === undefined) {
      setState(props.user);
    }
    const loadCommission = async () => {
      try {
        const res = await commissionService.findAll();
        let aux: any[] = [];
        let ct = 0;
        res.map((commission) => {
          ct = ct + commission.commission;
          if (commission.typeReferido === 'comprartir') {
            const commi = {
              x: 'Comprartir',
              y: commission.commission
            }
            aux.push(commi);

          } else {
            if (commission.typeReferido === 'vendedor') {
              const commi = {
                x: 'Vendedor',
                y: commission.commission
              }
              aux.push(commi);

            } else {
              if (commission.typeReferido === 'comprador') {
                const commi = {
                  x: 'Comprador',
                  y: commission.commission
                }
                aux.push(commi);

              }
            }
          }
          return '';
        });

        setCommissions(res);
        setCommissionPieData(aux)
        setCommissionTotal(ct);

      } catch (error) {
        console.log('Error', error);
        console.log(commissions);
      }

    }
    loadCommission();

  }, [props.user, state, commissions])

  return (
    <div>
      <Row gutter={[8, 8]} justify='center' align='stretch'>
        <Col xs={24} sm={24} md={12} lg={12}>
          <Suspense fallback={null}>
            <Card
              className='charcard-welcome'
              actions={[
                <span>
                  <MessageOutlined />
                  <Text
                    style={{ fontSize: 16 }}
                  >
                    {' 7 mensajes'}
                  </Text>
                </span>,
                <span>
                  <NotificationOutlined />
                  <Text
                    style={{ fontSize: 16 }}
                  >
                    {' 7 notificaciones'}
                  </Text>
                </span>,
                <span>
                  <EyeOutlined />
                  <Text
                    style={{ fontSize: 16 }}
                  >
                    {' 100 visitas'}
                  </Text>
                </span>,
              ]}
            >

              <Title
                style={{ fontSize: 16 }}
              >
                Bienvenido al dashboard de Prestamos Perico
              </Title>
              <div>
                <Title
                  style={{ fontSize: 16 }}
                >
                  {state?.firstName + ' ' + state?.lastName + ' '}
                </Title>

                <Button
                  type="link"
                  onClick={showDrawer}
                  key={state?.id}
                  style={{ marginLeft: -16.5 }}
                >
                  Ver perfil
                </Button>
              </div>

            </Card>

          </Suspense>
        </Col>
        <Col xs={24} sm={24} md={12} lg={12}>
          <Suspense fallback={null}>
            <ChartCard
              style={{ width: '100%' }}
              title={
                <Title
                  style={{ fontSize: 14 }}
                >
                </Title>
              }
            >
              <Row gutter={16}>
                <Col span={12}>
                  <Card>
                    <Statistic
                      title="Usuarios Activos"
                      value={11.28}
                      precision={2}
                      valueStyle={{ color: '#3f8600' }}
                      prefix={<ArrowUpOutlined />}
                      suffix="%"
                    />
                  </Card>
                </Col>
                <Col span={12}>
                  <Card>
                    <Statistic
                      title="Usuarios Inicativos"
                      value={9.3}
                      precision={2}
                      valueStyle={{ color: '#cf1322' }}
                      prefix={<ArrowDownOutlined />}
                      suffix="%"
                    />
                  </Card>
                </Col>
              </Row>
            </ChartCard>
          </Suspense>

        </Col>
      </Row>
      <Drawer
        width={500}
        placement="right"
        closable={false}
        onClose={showDrawer}
        visible={visible}
      >
        <Title
          style={{ fontSize: 16 }}
        >
          Datos Personales
        </Title>
        <Profil user={state} />

      </Drawer>
      <Row gutter={[8, 8]} justify='start' >
        <Col xs={24} sm={24} md={12} lg={12} >

          <Suspense fallback={null}>
            <Card
              title='Mapa de Incidencia de Prestamos Perico'
              style={{ height: 560 }}
            >
              <MapComprartir />
            </Card>
          </Suspense>
        </Col>
        <Col xs={24} sm={24} md={12} lg={12} >

          <Suspense fallback={null}>
            <ChartCard
              title={

                <Title
                  style={{ fontSize: 16 }}
                >
                  <img
                    alt="indicator"
                    style={{ width: 30, height: 30, marginLeft: 8, marginRight: 8, marginTop: - 5 }}
                    src={CommissionIcon}
                  />
                    Comisiones
                  </Title>
              }
              action={
                <Tooltip title="información">
                  <InfoCircleOutlined />
                </Tooltip>
              }
            >
              <Pie
                hasLegend
                title={<Title>Comisiones</Title>}
                subTitle={<Text> Total:</Text>}
                total={() => (
                  <Title
                    style={{ fontSize: 16 }}
                  >

                    ${commmissionTotal}
                  </Title>
                )}
                data={commissionPieData}
                valueFormat={val => <Text style={{ position: 'relative', left: 20 }}> ${Number(val).toFixed(2)}</Text>}
                height={320}
              />,
            </ChartCard>
          </Suspense>
        </Col>
      </Row>
    </div>
  );
}
export default MenuPage;