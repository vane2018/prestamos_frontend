import React from 'react';
import { MapboxScene, Marker } from '@antv/l7-react';
import LogoBag from '../../../resources/icons/logo-bag.svg';

function creatMarkers() {
    const markers = [];
    
    markers.push(<Marker key={1} lnglat={[-65.3, -24.1833]} children={<img src={LogoBag} alt='img/svg' height={20} width={20}/>}/>);
        
    return markers;
}

const MapComprartir = React.memo(function Map() {
  return (
    <MapboxScene
      map={{
        center: [ -53.616672, -20.1833 ],
        pitch: 0,
        style: 'light',
        zoom: 2
      }}
      style={{
        position: 'absolute',
        top: 58,
        left: 0,
        right: 0,
        bottom: 0
      }}
    >
        {creatMarkers()}
    </MapboxScene>
  );
});
export default MapComprartir;