import { AMapScene, Marker } from '@antv/l7-react';
import * as React from 'react';
import LogoBag from '../../../resources/icons/logo-bag.svg';

function creatMarkers() {
    const markers = [];
    
    markers.push(<Marker key={1} lnglat={[-65.3, -24.1833]} children={<img src={LogoBag} alt='img/svg' height={20} width={20}/>}/>);
        
    return markers;
}

const MapOreration = React.memo(function Map() {
    return (
        <AMapScene 
            map={{
                center: [-63.616672, -38.416097],
                pitch: 0,
                style: 'light',
                zoom: 0,
            }}
        >
            {creatMarkers()}
        </AMapScene >
    );
});
export default MapOreration;
