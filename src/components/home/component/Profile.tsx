import React, { useEffect } from 'react';
import { Row, Col,  Divider } from 'antd';
//import { IUser } from '../../../services/users/user.interface';

const Profile = (props: any) => {
    //const [state, setState] = useState<IUser>();

    useEffect(() => {
        
       // setState(props.user)
    }, [props.user])
    return (
        <div>
            <Divider />
            <Row>
                <Col span={12}>
                    <p >Nombre : {' ' + props.user.firstName + ' ' + props.user.lastName}</p>
                </Col>
                <Col span={12}>
                    <p >Email : {' ' + props.user.email}</p>
                </Col>
            </Row>
            <Row>
                <Col span={12}>
                    <p >Nick : {' ' + props.user.nickName}</p>
                </Col>
                <Col span={12}>
                    <p >Genero : {' ' + props.user.gender}</p>
                </Col>
            </Row>
            <Row>
                <Col span={12}>
                    <p >Telefon : {' ' + props.user.phone}</p>
                </Col>
            </Row>
        </div>
    );
}
export default Profile;