import React, { useState, useEffect, Suspense } from "react";
import moment from 'moment';
import { Row, Col, Typography, Tooltip } from 'antd';
import { ChartCard, MiniArea, Pie } from 'ant-design-pro/lib/Charts';
import { InfoCircleOutlined } from "@ant-design/icons";
import { MercadopagoService } from "../../services/mercadopago/mercadopago.service";
import { IMercadopago } from "../../services/mercadopago/mercadopago.interface";
import NumberInfo from 'ant-design-pro/lib/NumberInfo';
import CommissionIcon from '../../resources/icons/commission-icon.svg'
import 'ant-design-pro/dist/ant-design-pro.css';


const visitData: any = [];
const beginDay = new Date().getDate();
const mercadopagoService = new MercadopagoService();
const { Title, Text } = Typography;

for (let i = 0; i < 20; i += 1) {
    visitData.push({
        x: moment(new Date(beginDay + 1000 * 60 * 60 * 24 * i)).format('YYYY-MM-DD'),
        y: Math.floor(Math.random() * 100) + 10,
    });
}
const MercadopagoPayment = () => {
    const [mercadopago, setMercadopago] = useState<IMercadopago[]>();
    const [mercadopagoStatus, setMercadopagoStatus] = useState<any[]>();
    const [mercadopagoTransactionAmount, setMercadopagoTransactionAmount] = useState<String>();
    const [mercadopagoApplicationFee, setMercadopagoApplicationFee] = useState<String>();
    useEffect(() => {
        const mercadopagoFiendAll = async () => {           
            let mercadopagoStatus: any[] = [];
            let transactionAmount: number = 0;
            let applicationFee: number = 0;
            const resMercadopago: IMercadopago[] = await mercadopagoService.findAll();
            setMercadopago(resMercadopago);
            resMercadopago.forEach((mp: IMercadopago) => {
                switch (mp.status) {
                    case 'approved':
                        const paymentAp = {
                            x: 'Aprobados',
                            y: 1
                        }
                        transactionAmount += mp.transaction_amount;
                        applicationFee += Number(String(String(mp.fee_details).split(':')).split(',')[9]);
                        mercadopagoStatus.push(paymentAp);
                        break;
                    case 'rejected':
                        const paymentRe = {
                            x: 'Rechazados',
                            y: 1
                        }
                        mercadopagoStatus.push(paymentRe);
                        break;

                    default:
                        const paymentOt = {
                            x: 'Otros',
                            y: 1
                        }
                        mercadopagoStatus.push(paymentOt);
                        break;
                }
            });
            setMercadopagoStatus(mercadopagoStatus);
            setMercadopagoTransactionAmount(Number(transactionAmount).toFixed(2));
            setMercadopagoApplicationFee(Number(applicationFee).toFixed(2));
        }
        mercadopagoFiendAll();
    }, []);

    return (
        <Row gutter={[8, 8]}>
            <Col xs={24} sm={24} md={12} lg={8} >
                <Suspense fallback={null}>
                    <ChartCard
                        title={
                            <Title
                                style={{ fontSize: 16 }}
                            >
                                <img
                                    alt="indicator"
                                    style={{ width: 30, height: 30, marginLeft: 8, marginRight: 8, marginTop: - 5 }}
                                    src={CommissionIcon}
                                />
                                    Mercado Pagos: Estados de transacciones
                                </Title>
                        }
                        action={
                            <Tooltip title="información">
                                <InfoCircleOutlined />
                            </Tooltip>
                        }
                    >
                        <Pie
                            hasLegend
                            title={<Title>Mercado Pago</Title>}
                            subTitle={<Text style={{ fontSize: 15 }}>Nº Transacciones:</Text>}
                            total={() => (
                                <Title
                                    style={{ fontSize: 18, marginTop: 30 }}
                                >
                                    {mercadopago?.length}
                                </Title>
                            )}
                            data={mercadopagoStatus}
                            valueFormat={val => <Text > {val}</Text>}
                            height={300}
                        />
                    </ChartCard>
                </Suspense>
            </Col>
            <Col xs={24} sm={24} md={12} lg={8}>
                <Suspense fallback={null}>
                    <ChartCard
                        title={
                            <Title
                                style={{ fontSize: 16 }}
                            >
                                <img
                                    alt="indicator"
                                    style={{ width: 30, height: 30, marginLeft: 8, marginRight: 8, marginTop: - 5 }}
                                    src={CommissionIcon}
                                />
                                    Mercado Pagos: Monto de transacciones
                                </Title>
                        }
                        total={
                            <Title level={3} style={{ paddingTop: 10 }}>
                                $ {mercadopagoTransactionAmount}
                            </Title>
                        }
                        contentHeight={268}
                    >
                        <NumberInfo
                            subTitle={<Text style={{ fontSize: 15 }}>Aplicación Comprartir:</Text>}
                            total={
                            <Text>$ {mercadopagoApplicationFee}</Text>}

                        />

                        <MiniArea line height={180} data={visitData} />
                    </ChartCard>
                </Suspense>
            </Col>

        </Row>
    );
}
export default MercadopagoPayment;