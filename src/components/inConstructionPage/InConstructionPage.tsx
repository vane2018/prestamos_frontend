import React from 'react';
import { Result } from 'antd';


const InConstructionPage: React.FC<{}> = () => (

  <Result
    status="404"
    title="404"
    subTitle="Lo sentimos, la página que visitaste esta en contruccion."
    // extra={
    //   <Button type="primary" /* onClick={() => useHistory().push('/')} */>
    //     Back Home
    //   </Button>
    // }
  />
);

export default InConstructionPage;