import React, { useState, useEffect } from 'react';

//importacions de paquetes de diseño
import { Avatar, Typography, List, Button } from 'antd';

// importación servicio, interface de datos, imagenes , iconos y css
import { UserService } from '../../services/users/user.service';
import { IUser } from '../../services/users/user.interface';
import Profile from '../../resources/images/profile.svg';
import {Link} from 'react-router-dom'
import {useParams} from 'react-router-dom'
import { LoanService } from '../../services/loans/loan.service';
import { ILoan } from '../../services/loans/loan.interface';
import { Table,Modal, Input, Form  } from 'antd';
import axios from 'axios';
import { APIs } from '../../constants/Constants';



const loanService = new LoanService();
const userService = new UserService();

const LoanPage = () => {
  const [loans, setLoans] = useState([]);
  const [modalEditar, setModalEditar]=useState(false);
  const [modalEliminar, setModalEliminar]=useState(false);
  const [loanauxiliar, setLoanauxiliar]=useState(ILoan)


  const layout={
    labelCol:{
      span: 8
    },
    wrapperCol:{
      span: 16
    }
  };
  const { Item } = Form;

  

  
 
  const abrirCerrarModalEditar=()=>{
    setModalEditar(!modalEditar);
  }

  const abrirCerrarModalEliminar=()=>{
    setModalEliminar(!modalEliminar);
  }


  
  

  const handleChange=e=>{
    const {name, value}=e.target;
    setLoanauxiliar({...loanauxiliar,
    [name]: value});
    console.log(loanauxiliar);

  }

  function seleccionarPrestamo(loanauxiliar,caso){
    console.log("AUXILIAR",loanauxiliar);
    setLoanauxiliar(loanauxiliar);
      console.log("AUXILIAR",loanauxiliar);
    (caso==="Editar")?abrirCerrarModalEditar():abrirCerrarModalEliminar()
  }


  var  columns = [
    {
      title: 'Nombre',
      dataIndex:'userficticio',
      key:'userficticio',
  
    },
    {
      title: 'Apellido',
      dataIndex:'lastNameficticio',
      key:'lastNameficticio',
 
    },
 
    {
      title: 'Monto Total',
      dataIndex: 'montoTotal',
      key:'montoTotal',
 
    },
    {
      title: 'Monto',
      dataIndex: 'monto',
      key:'monto',
    

     
    },
    {
      title: 'Interes',
      dataIndex:'interes',
      key:'interes',
     

    },
    {
      title: 'Fecha',
      dataIndex:'createAt',
      key:'createAt',


    },
    {
      title: 'Prenda',
      dataIndex:'prenda',
      key:'prenda',
   

    },
     
    {
      title: 'Acciones',
      key:'acciones',
      render: (fila) => (
        <>
          <Button type="primary" onClick={() => seleccionarPrestamo(fila,"Editar")}>Editar</Button> {"   "}
          <Button type="primary" danger onClick={()=>seleccionarPrestamo(fila, "Eliminar")}>
            Eliminar
          </Button>
        </>
      ),
     
      /* render: () => <><Button type="primary">Editar</Button>{"   "}<Button type="primary" danger><Link to="">Eliminar</Link></Button></> */

    },
  ];
  

  const loadLoans = async () => {
    const res = await loanService.findAll('');
    setLoans(res);
  }

  console.log(loans);
  const peticionPut = () =>{
  

abrirCerrarModalEditar();

  }

  const borrar= async()=>{
    await loanService.removeLoans(loanauxiliar.id);
    abrirCerrarModalEliminar();
  }
  

  useEffect(() => {
    loadLoans();
    setLoanauxiliar(loans)
    
    

  }, []);
  console.log("loan COMPROBAR",loans);
  console.log("loanAUXILIAR COMPROBAR",loanauxiliar);

 
  

  

     
  

  
  
  
  return (

<div className="LoanPage">
    <h4>Prestamos</h4>
  
    <Table columns={columns} dataSource={loans} size="middle" />

    <Modal
      visible={modalEditar}
      title="Editar Artista"
      onCancel={abrirCerrarModalEditar}
      centered
      footer={[
        <Button onClick={abrirCerrarModalEditar}>Cancelar</Button>,
        <Button type="primary" onClick={()=>{loanService.updateLoan(loanauxiliar.id, loanauxiliar);abrirCerrarModalEditar()}}>Editar</Button>,
      ]}
      >
<Form {...layout}>
  <Item label="Monto Total">
    <Input name="montoTotal" type="number" onChange={handleChange} value={loanauxiliar && loanauxiliar.montoTotal}/>
  </Item>

  <Item label="Monto">
    <Input name="monto" onChange={handleChange} value={loanauxiliar && loanauxiliar.monto}/>
  </Item>

  <Item label="Interes">
    <Input name="interes" onChange={handleChange} value={loanauxiliar && loanauxiliar.interes}/>
  </Item>

  <Item label="Fecha">
    <Input name="createAt" onChange={handleChange} value={loanauxiliar && loanauxiliar.createAt}/>
  </Item>

  <Item label="Prenda">
    <Input name="prenda" onChange={handleChange} value={loanauxiliar && loanauxiliar.prenda}/>
  </Item>

  
</Form>
      </Modal>


          
      <Modal
      visible={modalEliminar}
      onCancel={abrirCerrarModalEliminar}
      centered
      footer={[
        <Button onClick={abrirCerrarModalEliminar}>No</Button>,
        <Button type="primary" danger onClick={borrar}>Sí</Button>,
      ]}
      >
Estás seguro que deseas eliminar al artista <b>{loanauxiliar && loanauxiliar.userficticio}</b>?
      </Modal>
 
    

  </div>


  

  );
}
export default LoanPage;