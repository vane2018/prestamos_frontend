import * as React from 'react';
import { useState, useEffect } from 'react';
import axios, { AxiosResponse } from 'axios';
import { APIs } from '../../constants/Constants';
import { IMercadoPago } from '../../mercadopago/mercadopago.interface';

const APP_ID = process.env.MERCADO_PAGO_APP_ID ? process.env.MERCADO_PAGO_APP_ID :
  process.env.NODE_ENV === 'production' ? '5848777206530091' : '5848777206530091';

const REDIRECT_URL = process.env.MERCADO_PAGO_APP_ID ? 'https://api.comprartir-staging.com.ar/api/v1/mercado-pago/redirect' :
  process.env.NODE_ENV === 'production' ? 'https://api.comprartir-staging.com.ar/api/v1/mercado-pago/redirect' : 'http://localhost:8080/api/v1/mercado-pago/redirect';

const URL_API_MERCADO_PAGO = 'https://auth.mercadopago.com.ar/authorization?client_id=' +
  APP_ID + '&response_type=code&platform_id=mp&redirect_uri=' + REDIRECT_URL;

interface Props {
  comercioId?: string;
  match: any;
  // usuario: Usuario;
}

enum StateMP {
  NO_CONECTED = 'no-conected',
  CONECTED = 'conected',
  REDIRECTING = 'redirecting',
  CARGANDO = 'cargando',
  ERROR = 'error'
}

export default function MercadoPago(props: Props) {
  
  const [idParam] = useState(props.match.params.idParam);
  const [stateMP, setstateMP] = useState(StateMP.NO_CONECTED);
  const [mercadoPago, setMercadoPago] = useState<IMercadoPago>({ id: '-' } as IMercadoPago);

  const fetchData = async () => {
    try {
      const res: AxiosResponse<IMercadoPago> =
        await axios.get(APIs.MERCADOPAGO + '/commerceId/' + props.comercioId);
      console.log('res.data', res.data);

      setMercadoPago(res.data);
      setstateMP(StateMP.CONECTED);
    } catch (error) {
      console.log('error en consulta mercado pago con id comercio');
      console.log(error);
      if (error.response && parseInt(error.response.status, 10) === 404) {
        setstateMP(StateMP.NO_CONECTED);
      }
    }
    return
  };

  const verifyMercadoPagoId = async (mercadoPagoId: string) => {
    try {
      console.log('mercadoPagoId', mercadoPagoId);
      console.log('props.comercioId', props.comercioId);
      
      const res: AxiosResponse<IMercadoPago> =
      await axios.put(`${APIs.MERCADOPAGO}/mercado-pago-id/${mercadoPagoId}/comercio-id/${props.comercioId}`);
      console.log('res.data', res.data);
      fetchData();
    } catch (error) {
      console.log('error en actualizacion de mercado pago con id comercio');
      console.log(error);
      setstateMP(StateMP.ERROR);
      if (error.response) {
        console.log(error.response);
      }
    }
  };

  const removeMercadoPago = async () => {
    try {
      await axios.delete(APIs.MERCADOPAGO + '/' + mercadoPago.id);
      setMercadoPago({ id: '-' } as IMercadoPago);
      setstateMP(StateMP.NO_CONECTED);
    } catch (error) {
      console.log('error al desvincular comercio.');
      console.log(error);
    }
  };

  useEffect(() => {
    const idParamArray = idParam.split('_');    
    if (idParamArray[0] === 'mercadoPagoId') {
      const mercadoPagoId = idParamArray[1];
      verifyMercadoPagoId(mercadoPagoId);
    } else if (idParam === 'error-in-create-credential') {
      setstateMP(StateMP.ERROR);
    } else if (idParam === 'mercado-pago-already-exist') {
      setstateMP(StateMP.CONECTED);
    } else {
      fetchData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleButton = () => {
    setstateMP(StateMP.REDIRECTING);
  }

  const handleButtonDesconectar = async () => {
    setstateMP(StateMP.CARGANDO);
    await removeMercadoPago();
  }

  return (
    <div className="block">
      <div className="block-title">
        <h2><i className="fa fa-plus" /> Mercado Pago</h2>
      </div>
      <div className="row">
        <div className=" col-sm-offset-1">
          <div className="block-section col-sm-4">
            {stateMP === StateMP.ERROR ?
              <h3 className="sub-header text-center"><strong>Ups no se pudo conectar, intenta de nuevo mas tarde.</strong></h3>
              : stateMP === StateMP.NO_CONECTED ?
                <div>
                  <h3 className="sub-header text-center"><strong>Conecta tu cuenta de Marcado Pago a Compartir!</strong></h3>
                  <p className="clearfix"><i className="fa fa-plus fa-5x text-primary pull-left"></i>Has click en el boton siguiente y permite que comprartir se vincule con tu cuenta.</p>
                  <p>
                    <a href={URL_API_MERCADO_PAGO} onClick={handleButton} className="btn btn-lg btn-primary btn-block">Conectar <i className="fa fa-arrow-right"></i></a>
                  </p>
                </div>
                : stateMP === StateMP.CONECTED ?
                  <h3 className="sub-header text-center">
                    <strong>Tienes tu cuenta conectada a Comprartir</strong>
                    <p>Este es tu id: {mercadoPago.id}</p>
                    <a href={'/'} onClick={handleButtonDesconectar} className="btn btn-lg btn-danger btn-block">Desconectar <i className="fa fa-times"></i></a>
                  </h3>
                  :
                  <h3 className="sub-header text-center"><strong>Redireccionando a Mercado pago...</strong></h3>
            }
          </div>
        </div>
      </div>
    </div>
  );
}