import React, { Fragment } from 'react';
import { Row, Spin } from 'antd';
const MercadopagoLoading = () => {
    return (
        <Fragment>
            <div style={{ backgroundColor: 'white' }}>
                <Row justify='center' style={{ paddingTop: 40 }}>
                    <Spin tip="Cargando...">
                    </Spin>
                </Row>
            </div>
        </Fragment>
    );
}
export default MercadopagoLoading;