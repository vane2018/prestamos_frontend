import React, { Fragment } from 'react';
import { Row, Typography } from 'antd';
const MercadoPagoPage = () => {
    return (
        <Fragment>
            <div style={{ backgroundColor: 'white' }}>
                <Row justify='center' style={{paddingTop: 40}}>
                    <Typography.Text style={{ fontSize: 20, fontWeight: 'bold', color: 'black', paddingLeft:20, paddingRight: 20 }}>
                        Se ha accedido con Exito a MercadoPago
                    </Typography.Text>
                </Row>
                <Row justify='center'>
                    <Typography.Text style={{ fontSize: 16, color: 'black', paddingLeft:20, paddingRight: 20 }}>
                        Has click en el boton Aceptar y permite asociar tu cuenta de MercadoPago a Comprartir
                    </Typography.Text>
                </Row>
            </div>
        </Fragment>
    );
}
export default MercadoPagoPage;