import React, { useState, useEffect } from "react";
import { Typography, Tabs, List, Space, Button, message } from 'antd';
import { DollarOutlined } from "@ant-design/icons";
import { CollectCommissionService } from "../../services/collectcommission/collectcommission.service";
import { ICollectCommission } from "../../services/collectcommission/collectcommission.interface";
import 'ant-design-pro/dist/ant-design-pro.css';

const collectCommissionService = new CollectCommissionService();
const { Text } = Typography;
const { TabPane } = Tabs;
const key = 'updatable';

const IconText = ({ icon, text }: any) => (
    <Space>
        {React.createElement(icon)}
        {text}
    </Space>
);


const CommissionPage = () => {
    //const [collectCommission, setCollectCommission] = useState<ICollectCommission[]>();
    const [requestPaymentCommission, setRequestPaymentCommission] = useState<ICollectCommission[]>([]);
    const [paymentMadeCommission, setPaymentMadeCommission] = useState<ICollectCommission[]>([]);

    useEffect(() => {
        const collectCommissionFind = async () => {
            let paymentMadeCommission: any[] = [];
            let requestPaymentCommission: any[] = [];
            const resCollectCommission: ICollectCommission[] = await collectCommissionService.findAll();
            console.log('resCollectCommission', resCollectCommission)
            resCollectCommission.forEach((cc: ICollectCommission) => {
                switch (cc.estado) {
                    case 'Cobro Solicitado':
                        requestPaymentCommission.push(cc);
                        break;
                    case 'Comision Pagada':
                        paymentMadeCommission.push(cc)
                        break;

                    default:
                        break;
                }
            });
            //setCollectCommission(resCollectCommission);
            setPaymentMadeCommission(paymentMadeCommission);
            setRequestPaymentCommission(requestPaymentCommission);
        }
        collectCommissionFind();
    }, []);

    const onClickChangeStatus = async (cc: ICollectCommission) => {
        const statusParam = {
            estado: "Comision Pagada"
        }
        try {
            await collectCommissionService.editStatusByIdParam(cc.id, statusParam);
            message.loading({ content: 'Cargando...', key });
            setTimeout(() => {
                message.success({ content: 'El pago de la comisión se realizo con éxito!', key, duration: 2 });
            }, 1000);
            setTimeout(() => {
                window.location.reload();
            }, 1500);
        } catch (error) {
            message.loading({ content: 'Cargando...', key });
            setTimeout(() => {
                message.error({ content: 'El pago de la comisión no se ha realizo. Intentelo en unos momentos', key, duration: 2 });
            }, 1000);
        }
    }
    return (
        <Tabs defaultActiveKey="1" type="card" size={"large"}>
            <TabPane tab="Lista de solicitudes de cobro de comisiones" key="1">
                <List
                    itemLayout="vertical"
                    size="large"
                    pagination={{
                        onChange: page => {
                            console.log(page);
                        },
                        pageSize: 5,
                    }}
                    dataSource={requestPaymentCommission}
                    renderItem={(item: ICollectCommission) => (
                        <List.Item
                            key={item.id}
                            actions={[
                                <Button onClick={() => onClickChangeStatus(item)} >
                                    <IconText icon={DollarOutlined} text={<Text>Pagar Comisión </Text>} />
                                </Button>
                            ]}
                        >
                            <List.Item.Meta
                                title={
                                    <Space direction='vertical'>
                                        <Text>Nombre: {item.user.firstName + ' ' + item.user.lastName}</Text>
                                        <Text>Cbu/Cvu: {item.user.cbu}</Text>
                                        <Text style={{ fontWeight: 'bold' }}>Monto a Cobrar: ${item.monto} </Text>
                                    </Space>
                                }
                            />
                        </List.Item>
                    )}
                />
            </TabPane>
            <TabPane tab="Lista de comisiones pagadas" key="2">
                <List
                    itemLayout="vertical"
                    size="large"
                    pagination={{
                        onChange: page => {
                            console.log(page);
                        },
                        pageSize: 5,
                    }}
                    dataSource={paymentMadeCommission}
                    renderItem={(item: ICollectCommission) => (
                        <List.Item
                            key={item.id}

                        >
                            <List.Item.Meta
                                title={
                                    <Space direction='vertical'>
                                        <Text>Nombre: {item.user.firstName + ' ' + item.user.lastName}</Text>
                                        <Text>Cbu/Cvu: {item.user.cbu}</Text>
                                        <Text style={{ fontWeight: 'bold' }}>Monto a Cobrar: ${item.monto} </Text>
                                    </Space>
                                }
                            />
                        </List.Item>
                    )}
                />
            </TabPane>
        </Tabs>
    );
}
export default CommissionPage;