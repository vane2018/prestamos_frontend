
import React, { useEffect, useState } from 'react';
import {
    Form,
    Input,
    Select,
    Checkbox,
    Button,
    message,
    Row,
    Typography,
    Col,
} from 'antd';
import './Register.css';
import { UserRegisterDto } from '../../services/users/user.register.dto';
import { LoginService } from '../../services/login/login.service';
import { UserService } from '../../services/users/user.service';
import {useParams} from 'react-router-dom'
import { UserEditDto } from '../../services/users/user.edit.dto';
import { value } from 'numeral';
import { IUser } from '../../services/users/user.interface';

const { Option } = Select;
// const AutoCompleteOption = AutoComplete.Option;
const { Title } = Typography;
//const [miuser,setMiuser] =useState();
const userService = new UserService();




const RegisterEdit = () => {
    const [form] = Form.useForm();
    const [users, setUsers] = useState({});
    const { Id} = useParams();
    let miid= '"'+Id+'"' 
    console.log("parametro",miid);
     

     const loadUsers = async () => {
       
        const res = await userService.findOneByIdParam(Id);
        setUsers(res);
      }
  

useEffect(() => {
    loadUsers(miid);
  
 
}, [Id]);

form.setFieldsValue({
    firstName: users.firstName,
    lastName: users.lastName,
    dni: users.dni, 
   
    localidad: users.localidad, 
    provincia:users.provincia, 
pais:users.pais, 
gender: users.gender,
barrio: users.barrio,
calle:users.calle,
    numero:users.numero,
    phone: users.phone,
   

    

  });





console.log("USER ES",users.localidad);
var nombreuser=users.firstName;
console.log((nombreuser));




    const onFinish = async (values) => {
        message.loading('Iniciando Sesión..', 0.5);
        console.log('Received values of form: ', values);

       
        try {
            const user = new UserEditDto(
               
               
                values.firstName, 
                values.lastName, 
                values.dni, 
                values.localidad, 
                values.provincia, 
                values.pais, 
                values.phone,
                values.gender,
                values.barrio,
                values.calle,
                values.numero,
              
                )
       
                
            console.log('received values user a cambiar', user.$id);
            console.log("VERIVICO",user);
       
            const res = await userService.updateUser(miid,user);
            console.log(res);
            message.success('Su registro fue exitoso..', 0.5);
            /* props.history.push('/login'); */
        } catch (error) {
            message.error('Error de registro: ' + error, 2.5);
        }
    };

    return (
        <>{
            users ? 
         
            <div className={'root'}>
            <Row justify='space-around' >
                <Title level={2}> Formulario de Registro del Cliente</Title>
            </Row>
            <Row justify='space-around'>
                <Col className={'form-col'}>
                    <Form
                        labelAlign='left'
                        labelCol={{
                            xs: { span: 24 },
                            sm: { span: 5 }
                        }}
                        form={form}
                        name="register"
                        onFinish={onFinish}
                        scrollToFirstError
                        size={'large'}
                       
                    >
                        <Form.Item
                            name="firstName"
                            label="Nombre"
                           
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su nombre!',
                                },
                            ]}
                            
                           
                        >
                            <Input/>
                        </Form.Item>

                        <Form.Item
                            name="lastName"
                            label="Apellido"
                          
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su apellido!',
                                },
                            ]}
                        
                        >
                            <Input/>
                        </Form.Item>

                        <Form.Item
                            name="dni"
                            label="DNI"
                         
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su dni!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        

                        <Form.Item
                            name="localidad"
                            label="Localidad"
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su localidad!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            name="provincia"
                            label="Provincia"
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su proincia!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            name="pais"
                            label="Pais"
                            
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su pais!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            name="gender"
                            label="Genero"
                            rules={[
                                {
                                    required: true,
                                    message: 'Por favor seleccione una opción'
                                }
                            ]}>
                            <Select
                                placeholder="Seleccione una opción..."
                                allowClear
                            >
                                <Option value="Masculino">Masculino</Option>
                                <Option value="Femenino">Femenino</Option>
                                <Option value="Otro">Otro</Option>
                            </Select>
                        </Form.Item>

                        <Form.Item
                            name="barrio"
                            label='Barrio'
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Por favor ingrese su domicilio!', 
                                    whitespace: true 
                                }
                            ]}
                        >
                            <Input />
                        </Form.Item>


                        <Form.Item
                            name="calle"
                            label="Calle"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Por favor ingrese su calle!' 
                                },
                            ]}
                        >
                            <Input style={{ width: '100%' }} />
                        </Form.Item>
                        <Form.Item
                            name="numero"
                            label="Numero"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Por favor ingrese numero!' 
                                },
                            ]}
                        >
                            <Input style={{ width: '100%' }} />
                        </Form.Item>
                        <Form.Item
                            name="phone"
                            label="Télefono"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Por favor ingrese numero de télefono!' 
                                },
                            ]}
                        >
                            <Input style={{ width: '100%' }} />
                        </Form.Item>
                       
                       
                        
                        <Form.Item 
                         wrapperCol={{ 
                            xs: {
                                span: 24,
                                offset: 0,
                            },
                            sm: {
                                span: 24,
                                offset: 5,
                            },
                         }}
                         >
                            <Button 
                            type="primary" 
                            htmlType="submit" block>
                                Registrar
                            </Button>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </div >
        :
        <h1>Cargando</h1>
        }
        </>
    
       
    );
};
export default RegisterEdit;