
import React from 'react';
import {
    Form,
    Input,
    Select,
    Checkbox,
    Button,
    message,
    Row,
    Typography,
    Col,
} from 'antd';
import './Register.css';
import { UserRegisterDto } from '../../services/users/user.register.dto';
import { LoginService } from '../../services/login/login.service';

const { Option } = Select;
// const AutoCompleteOption = AutoComplete.Option;
const { Title } = Typography;
const loginService = new LoginService();

const Register = (props: { history: string[]; }) => {
    const [form] = Form.useForm();

    const onFinish = async (values: any) => {
        message.loading('Iniciando Sesión..', 0.5);
        console.log('Received values of form: ', values);
        try {
            const user = new UserRegisterDto(
                values.password, 
                values.email, 
                values.firstName, 
                values.lastName, 
                values.dni, 
                values.localidad, 
                values.pais, 
                values.provincia, 
          
                values.phone,
                values.gender,
                values.barrio,
                values.calle,
                values.numero,
                ''
                )
            console.log('received values user admin dto', user);
            const res = await loginService.createUser(user);
            console.log(res);
            message.success('Su registro fue exitoso..', 0.5);
            props.history.push('/login');
        } catch (error) {
            message.error('Error de registro: ' + error, 2.5);
        }
    };

    return (
        <div className={'root'}>
            <Row justify='space-around' >
                <Title level={2}> Formulario de Registro del Cliente</Title>
            </Row>
            <Row justify='space-around'>
                <Col className={'form-col'}>
                    <Form
                        labelAlign='left'
                        labelCol={{
                            xs: { span: 24 },
                            sm: { span: 5 }
                        }}
                        form={form}
                        name="register"
                        onFinish={onFinish}
                        scrollToFirstError
                        size={'large'}
                    >
                        <Form.Item
                            name="firstName"
                            label="Nombre"
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su nombre!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            name="lastName"
                            label="Apellido"
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su apellido!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            name="email"
                            label="Email"
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su Email!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            name="dni"
                            label="DNI"
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su dni!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>


                        <Form.Item
                            name="localidad"
                            label="Localidad"
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su localidad!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>


                        <Form.Item
                            name="pais"
                            label="Pais"
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su Pais!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>


                        <Form.Item
                            name="provincia"
                            label="Provincia"
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su povincia!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            name="gender"
                            label="Genero"
                            rules={[
                                {
                                    required: true,
                                    message: 'Por favor seleccione una opción'
                                }
                            ]}>
                            <Select
                                placeholder="Seleccione una opción..."
                                allowClear
                            >
                                <Option value="Masculino">Masculino</Option>
                                <Option value="Femenino">Femenino</Option>
                                <Option value="Otro">Otro</Option>
                            </Select>
                        </Form.Item>

                        <Form.Item
                            name="domicilio"
                            label='Domicilio'
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Por favor ingrese su domicilio!', 
                                    whitespace: true 
                                }
                            ]}
                        >
                            <Input />
                        </Form.Item>


                        <Form.Item
                            name="calle"
                            label="Calle"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Por favor ingrese su calle!' 
                                },
                            ]}
                        >
                            <Input style={{ width: '100%' }} />
                        </Form.Item>
                        <Form.Item
                            name="numero"
                            label="Numero"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Por favor ingrese numero!' 
                                },
                            ]}
                        >
                            <Input style={{ width: '100%' }} />
                        </Form.Item>
                        <Form.Item
                            name="phone"
                            label="Télefono"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Por favor ingrese numero de télefono!' 
                                },
                            ]}
                        >
                            <Input style={{ width: '100%' }} />
                        </Form.Item>
                       
                       
                        <Form.Item
                            wrapperCol={{ 
                                xs: {
                                    span: 24,
                                    offset: 0,
                                },
                                sm: {
                                    span: 24,
                                    offset: 5,
                                },
                             }}
                            name="agreement"
                            valuePropName="checked"
                            rules={[
                                { validator: (_, value) => value ? Promise.resolve() : Promise.reject('Por favoro acepte los terminos y condiciones') },
                            ]}
                            
                        >
                            <Checkbox>
                                Acepto <a href="/">terminos y condiciones</a>
                            </Checkbox>
                        </Form.Item>
                        <Form.Item 
                         wrapperCol={{ 
                            xs: {
                                span: 24,
                                offset: 0,
                            },
                            sm: {
                                span: 24,
                                offset: 5,
                            },
                         }}
                         >
                            <Button 
                            type="primary" 
                            htmlType="submit" block>
                                Registrar
                            </Button>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </div >
    );
};
export default Register;