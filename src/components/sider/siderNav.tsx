import React from 'react';
import { Menu } from 'antd';
import {
    DashboardOutlined,
    UserOutlined,
    AppstoreOutlined,
    ShopOutlined,
    ShoppingOutlined,
    ControlOutlined,
    ReconciliationOutlined,
    CompassOutlined,
    AuditOutlined,
    CreditCardOutlined
} from '@ant-design/icons';
import { useHistory } from 'react-router';
import logoprestamo from '../../resources/images/logoprestamo.png';
import './sinav.css'
import Register from '../register/Register';

const { SubMenu } = Menu;

const SideNav = () => {
    const history = useHistory();

    const handleDashClick = () => {
        history.push('/');
    }

    const handleUserClick = () => {
        history.push('/user');
    }

    const handleCommerceClick = () => {
        history.push('/commerce');
    }

    const handleLoan2Click = () => {
        history.push('/loans');
    }

    const handleAppClick = () => {
        history.push('/app');
    }

    const handleInConstructionClick = () => {
        history.push('/incontruction-page')
    }

    const handleMercadopagoClick = () => {
        history.push('/mercado-pago-inf')
    }

    const handleCommissionClick = () => {
        history.push('/register')
    }
    const handleLoanClick = () => {
        history.push('/loan')
    }
    return (
        <div>
            <div
                style={{
                    display: 'flex',
                    height: "40px",
                    marginLeft:"70px",
                    alignItems: 'center'
                }
                }
            >
                <img src={logoprestamo} alt={'img/svg'}></img>
            </div>
            <Menu
                theme='dark'
                mode='inline'
                defaultSelectedKeys={['dash']}
            >
                <SubMenu
                    key="dash"
                    title={
                        <span>
                            <DashboardOutlined />
                            <span>Dashboard</span>
                        </span>
                    }
                    onTitleClick={handleDashClick}
                >
                    <Menu.Item
                        key="analysis"
                        icon={<ReconciliationOutlined />}
                        onClick={() => history.push('/dashboard/analysis')}
                    >
                        Análisis
                    </Menu.Item>
                    <Menu.Item
                        key="monitoring"
                        icon={<CompassOutlined />}
                        onClick={handleInConstructionClick}
                    >
                        Supervisión
                    </Menu.Item>
                    <Menu.Item
                        key="workplace"
                        icon={<ControlOutlined />}
                        onClick={handleInConstructionClick}
                    >
                        Espacio de trabajo
                    </Menu.Item>
                </SubMenu>
                <Menu.Item
                    key="customers"
                    icon={<UserOutlined />}
                    onClick={handleUserClick}
                >
                    Usuarios
                </Menu.Item>
              {/*   <Menu.Item
                    key="commerces"
                    icon={<ShopOutlined />}
                    onClick={handleCommerceClick}
                >
                    Comercios
                </Menu.Item> */}
                <Menu.Item
                    key="loans"
                    icon={<ShoppingOutlined />}
                    onClick={handleLoan2Click}
                >
                    Prestamos
                </Menu.Item> 
                <Menu.Item
                    key="register"
                    icon={<AuditOutlined />}
                    onClick={handleCommissionClick}
                >
                    Registrar Usuario
                </Menu.Item>  
                <Menu.Item
                    key="loan"
                    icon={<AuditOutlined />}
                    onClick={handleLoanClick}
                >
                    Registrar Prestamos
                </Menu.Item>     

                <Menu.Item
                    key="app"
                    icon={<AppstoreOutlined />}
                    onClick={handleAppClick}
                >
                    Aplicación
                </Menu.Item>
            </Menu>
        </div>
    );
}
export default SideNav;