import React, { useState, useEffect } from 'react';

//importacions de paquetes de diseño
import { Avatar, Typography, List } from 'antd';

// importación servicio, interface de datos, imagenes , iconos y css
import { UserService } from '../../services/users/user.service';
import { IUser } from '../../services/users/user.interface';
import Profile from '../../resources/images/profile.svg';
import {Link} from 'react-router-dom'
import {useParams} from 'react-router-dom'

const userService = new UserService();

const UserPage = () => {
  const [users, setUsers] = useState<IUser[]>();

  const loadUsers = async () => {
    const res: any = await userService.findAll('');
    setUsers(res);
  }

  useEffect(() => {
    loadUsers();

  }, []);
  return (
    <List
      grid={{
        gutter: 16,
        xs: 1,
        sm: 1,
        md: 2,
        lg: 3,
        xl: 3,
        xxl: 3,
      }}
      dataSource={users}
      renderItem={item => (
      <List.Item key={item.nickName}>
         <Link to={`/userdesc/${item.id}`}>  <List.Item.Meta
            avatar={
              (item?.profilePic === '' || item.profilePic === null) ?
                <Avatar
                  size="small"
                  src={Profile}
                  style={{ backgroundColor: 'grey' }}
                /> :
                <Avatar
                  size="small"
                  src={item?.profilePic}

                />
            }
            title={<Typography.Text strong >{item.nickName}</Typography.Text>}
            description={<Typography.Text>{item.email}</Typography.Text>}
          /></Link> 

          <div style={{ marginLeft: 40 }}>
            <Typography.Text >{item.firstName + ' ' + item.lastName}</Typography.Text>
          </div>
        </List.Item>
      )}

    />

  );
}
export default UserPage;