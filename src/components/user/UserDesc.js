import React,{useState, useEffect} from 'react'
import 'antd/dist/antd.css';
import { Typography } from 'antd';
import { List } from 'antd';
import {useParams} from 'react-router-dom'
import { Row, Col, Divider } from 'antd';
import { UserService } from '../../services/users/user.service';
import { Card } from 'antd';
import { IUser } from '../../services/users/user.interface';
import { Layout, Avatar, Dropdown, Menu, Space } from 'antd';
import { Descriptions,Button } from 'antd';
import {DeleteFilled  ,EditFilled } from '@ant-design/icons';
import {Link} from 'react-router-dom';
const userService = new UserService();
const UserDesc = () => {
 const [user, setUser] = useState({})
const [user2, setuser2] = useState([]);
/*  type QuizParams = {

  id: string;
}; */

// In order to implement that, I'd apply my type to the hook when calling it.

const  {id } = useParams();
  console.log("compruebo id",id);
  const id2= '"'+id+'"'
 
 

  const loadUsers = async () => {
   
      const res= await userService.findOneByIdParam(id);
      setTimeout(() =>{  setUser(res)},1000);
  
    
    }
 const onRemoveUser = async(id)=>{
  const res= await userService.removeUser(id);
  setTimeout(() =>{  setUser(res)},1000);
 }
  
    useEffect(() => {
   

      loadUsers();
 
   
    
   
    }, [id])
  
  console.log("el user es",user.firstName);
  
    return (
      <>
		<Row>
      <Col span={24}>
		<Descriptions title="Informacion del Usuario" layout="vertical">
		<Descriptions.Item label="Nombre">{user?.firstName}</Descriptions.Item>
		<Descriptions.Item label="Apellido">{user?.lastName}</Descriptions.Item>
		<Descriptions.Item label="Dni">{user?.dni}</Descriptions.Item>
		<Descriptions.Item label="Direccion">
		{user?.barrio} {user?.calle}  {user?.numero}
		</Descriptions.Item>
		<Descriptions.Item label="Teléfono">{user?.localidad}</Descriptions.Item>
		<Descriptions.Item label="Localidad">{user?.localidad}</Descriptions.Item>
   <Descriptions.Item >{"     "}</Descriptions.Item>
    {/*  <Descriptions.Item ><div style={{fontSize: "20px",paddingLeft:"280px"}}><DeleteFilled /></div></Descriptions.Item>
    <Descriptions.Item ><div style={{fontSize: "20px"}}><EditFilled /></div></Descriptions.Item> */}
    </Descriptions>
    </Col>
    </Row>
    <Row gutter={[32,32]}>
      <Col xs={12} sm={8} md={6} lg={4}><div style={{backgroundColor:'white', color:'white',}}>columna</div></Col>
      <Col xs={12} sm={8} md={6} lg={4}><div style={{backgroundColor:'white', color:'white'}}>columna</div></Col>
      <Col xs={12} sm={8} md={6} lg={4}><div style={{backgroundColor:'white', color:'white'}}>columna</div></Col>
      <Col xs={12} sm={8} md={6} lg={4}><div style={{backgroundColor:'white', color:'white'}}>columna</div></Col>       
      <Col xs={12} sm={8} md={6} lg={4}><div style={{backgroundColor:'white', color:'black',fontSize: "20px", paddingLeft:'90px'}}><Button onClick={() => onRemoveUser(user?.id)} ><DeleteFilled />     </Button></div></Col>
      <Col xs={12} sm={8} md={6} lg={4}><div style={{backgroundColor:'white', fontSize: "20px",color:'black'}}><Button><Link to={`/registerEdit/${user?.id}`}><EditFilled /></Link></Button></div></Col>
      </Row>


    </>
    )
}

export default UserDesc




