import React from 'react';
import { Result } from 'antd';


const NoFoundPage: React.FC<{}> = () => (

  <Result
    status="404"
    title="404"
    subTitle="Lo sentimos, la página que visitaste no existe."
    // extra={
    //   <Button type="primary" /* onClick={() => useHistory().push('/')} */>
    //     Back Home
    //   </Button>
    // }
  />
);

export default NoFoundPage;