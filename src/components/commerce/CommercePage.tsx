import React, { useState, useEffect } from 'react';

//importacion de componentes de diseños
import { Avatar, Typography } from 'antd';
import { List } from 'antd';

//importación de servicios, interface de datos, imagenes, iconos y css
import { CommerceService } from '../../services/commerces/commerce.service';
import { ICommerce } from '../../services/commerces/commerce.interface';
import AvatarCommerce from '../../resources/icons/sell-icon.svg'

const commerceService = new CommerceService();

const CommercePage = () => {
    const [commerces, setCommerces] = useState<ICommerce[]>();

    const loadCommerces = async () => {
        const res: any = await commerceService.findAll();
        setCommerces(res);
    }

    useEffect(() => {
        loadCommerces();

    }, []);
    return (
        <List
            grid={{
                gutter: 16,
                xs: 1,
                sm: 1,
                md: 2,
                lg: 3,
                xl: 3,
                xxl: 3,
            }}
            dataSource={commerces}
            renderItem={item => (
                <List.Item key={item.name}>
                    <List.Item.Meta
                        avatar={
                            <Avatar shape="square" src={AvatarCommerce} />
                        }
                        title={<Typography.Text strong >{item.name}</Typography.Text>}
                        description={<Typography.Text>{'Propietario: ' + item.user.nickName }</Typography.Text>}
                    />

                    <div style={{ marginLeft: 48 }}>
                        <Typography.Text >{'Dirección ' + item.adress}</Typography.Text>
                    </div>
                </List.Item>
            )}

        />

    );
}
export default CommercePage;