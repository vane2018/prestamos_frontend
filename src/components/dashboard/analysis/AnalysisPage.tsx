import React, { useState, useEffect, Suspense } from "react";
import { ChartCard, MiniArea, Bar, Pie } from 'ant-design-pro/lib/Charts';

import NumberInfo from 'ant-design-pro/lib/NumberInfo';
import { Row, Col } from 'antd';

import moment from 'moment';
import { IUser } from "../../../services/users/user.interface";
import { UserService } from "../../../services/users/user.service";
import Title from "antd/lib/typography/Title";
import { ICommerce } from "../../../services/commerces/commerce.interface";
import { CommerceService } from "../../../services/commerces/commerce.service";
import 'ant-design-pro/dist/ant-design-pro.css';
import { IPurchase } from "../../../services/purchases/purchase.interface";
import { PurchaseService } from "../../../services/purchases/purchase.service";


const visitData: any = [];
const beginDay = new Date().getDate();
const userService = new UserService();
const commerceService = new CommerceService();
const purchaseService = new PurchaseService();
const salesData: any = [];

for (let i = 0; i < 20; i += 1) {
    visitData.push({
        x: moment(new Date(beginDay + 1000 * 60 * 60 * 24 * i)).format('YYYY-MM-DD'),
        y: Math.floor(Math.random() * 100) + 10,
    });
}


for (let i = 0; i < 12; i += 1) {
    salesData.push({
        x: `${i + 1} mes`,
        y: Math.floor(Math.random() * 1000) + 200,
    });
}

const AnalysisPage = () => {
    const [users, setUsers] = useState<IUser[]>();
    const [commerces, setCommerces] = useState<ICommerce[]>();
    const [purchases, setPurchases] = useState<IPurchase[]>();
    // const [countSeller, setCountSeller] = useState(0)

    const loadUsers = async () => {
        const res: any = await userService.findAll('');
        setUsers(res);
    }
    const loadCommerces = async () => {
        const res: any = await commerceService.findAll();
        setCommerces(res);
    }
    const loadPurchases = async () => {
        const res: any = await purchaseService.findAll();
        setPurchases(res);
    }

    const userWithReferrer = () => {
        let count = 0;
        users?.map((user: IUser, index: number) => {

            if (user.nickReferrer !== null)  {
                if (user.nickReferrer !== '') {
                count += 1;
                }               
            }
        })
        return count;
    }

    const purchaseForDay = () => {

        let pfd = 0;
        let dn = new Date().toJSON();
        var dateNow = new Date(dn).toLocaleString('en-GB', { timeZone: 'America/Argentina/Buenos_Aires' });
        dateNow = dateNow.split(',')[0];

        purchases?.map((purchase: IPurchase, index: number) => {
            var date = new Date(purchase.date).toLocaleString('en-GB', { timeZone: 'America/Argentina/Buenos_Aires' });
            date = date.split(',')[0];
            if (dateNow === date) {

                pfd = pfd + 1;
            }

        })

        return (pfd);
    }
    const purchaseForDayPercent = () => {

        let pt = purchases?.length;
        if (pt === undefined || pt === 0) {
            pt = 1;
        }

        return (purchaseForDay() / pt * 100);
    }
    useEffect(() => {
        loadUsers();
        loadCommerces();
        loadPurchases();

    }, []);

    return (
        <Row gutter={[8, 8]}>
            <Col xs={24} sm={24} md={8} lg={8}>
                <Suspense fallback={null}>
                    <ChartCard
                        title="Usuarios Registrados:"
                        total={
                            <Title level={3}>
                                {users?.length}
                            </Title>
                        }
                        contentHeight={120}
                    >
                        <NumberInfo
                            subTitle={<span>Usuarios con Referidos</span>}
                            total={userWithReferrer()}
                            status="up"
                        />
                        <MiniArea line height={45} data={visitData} />
                    </ChartCard>
                </Suspense>
            </Col>
            <Col xs={24} sm={24} md={8} lg={8}>
                <Suspense fallback={null}>
                    <ChartCard
                        title="Comercios Registrados:"
                        total={
                            <Title level={3}>
                                {commerces?.length}
                            </Title>
                        }
                        contentHeight={120}
                    >

                        <MiniArea line height={85} data={visitData} />

                    </ChartCard>
                </Suspense>
            </Col>
            <Col xs={24} sm={24} md={8} lg={8}>
                <Suspense fallback={null}>
                    <ChartCard
                        title="Ventas:"
                        total={
                            <Title level={3}>
                                {purchases?.length}
                            </Title>
                        }
                        contentHeight={120}
                    >

                        <Pie
                            percent={purchaseForDayPercent()}
                            subTitle={'Ventas por dia: ' + purchaseForDay()}
                            total={Number(purchaseForDayPercent()).toFixed(2) + '%'}
                            height={200}
                        />

                    </ChartCard>
                </Suspense>
            </Col>
            <Col xs={24} sm={24} md={24} lg={24}>
                <Bar height={250} title="Historial de ventas por mes" data={salesData} />
            </Col>
        </Row>
    );
}
export default AnalysisPage