
import React,{ useEffect,useState} from 'react';
import {
    Form,
    Input,
    Select,
    Checkbox,
    Button,
    message,
    Row,
    Typography,
    Col,
} from 'antd';
import './RegisterLoan.css';
import { LoanRegisterDto } from '../../services/loans/loan.register.dto';
import { LoginService } from '../../services/login/login.service';
import { LoanService } from '../../services/loans/loan.service';
import { ILoan } from '../../services/loans/loan.interface';
import { UserService } from '../../services/users/user.service';
import { IUser } from '../../services/users/user.interface';

const { Option } = Select;
// const AutoCompleteOption = AutoComplete.Option;
const { Title } = Typography;
const loanService = new LoanService();
const userService = new UserService();

const RegisterLoan = (props: { history: string[]; }) => {
    const [form] = Form.useForm();

    const onFinish = async (values: any) => {
        message.loading('Iniciando Sesión..', 0.5);
        console.log('Received values of form: ', values);
        try {
            const loan = new LoanRegisterDto(
                values.date,
                values.monto,
                values.interes,
                values.montoTotal,
                values.user,
                values.prenda,
                values.origen,
                values.descripcion,
             
                )
            console.log('received values user admin dto', loan);
            const res = await loanService.createLoan(loan);
            console.log("user seleccionado",res);
            message.success('Su registro fue exitoso..', 0.5);
            props.history.push('/login');
        } catch (error) {
            message.error('Error de registro: ' + error, 2.5);
        }
    };
    const [users, setUsers] = useState<IUser[]>();

    const loadLoan = async () => {
        const res: any = await userService.findAll('');
        setUsers(res);
    }

    useEffect(() => {
        loadLoan();

    }, []);
    return (
        <div className={'root'}>
            <Row justify='space-around' >
                <Title level={2}> Formulario de Registro de Prestamos</Title>
            </Row>
            <Row justify='space-around'>
                <Col className={'form-col'}>
                    <Form
                        labelAlign='left'
                        labelCol={{
                            xs: { span: 24 },
                            sm: { span: 5 }
                        }}
                        form={form}
                        name="register"
                        onFinish={onFinish}
                        scrollToFirstError
                        size={'large'}
                    >
                        <Form.Item
                            name="monto"
                            label="Monto"
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese el monto!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            name="interes"
                            label="Interes"
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese el interes!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            name="montoTotal"
                            label="MontoTotal"
                            rules={[
                                
                                {
                                    required: true,
                                    message: 'Por favor ingrese el Monto Total!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            name="user"
                            label="Usuario"
                            rules={[
                                {
                                    required: true,
                                    message: 'Por favor seleccione un usuario'
                                }
                            ]}>
                            <Select
                                placeholder="Seleccione una opción..."
                                allowClear
                            >
                                console.log(users);
                                
                                {users?.map(user => <Option value={`${user.id}`}>{user.firstName}</Option>)}
                               
                              
                            </Select>
                        </Form.Item>

                        <Form.Item
                            name="prenda"
                            label='Prenda'
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Por favor ingrese la Prenda!', 
                                    whitespace: true 
                                }
                            ]}
                        >
                            <Input />
                        </Form.Item>


                        <Form.Item
                            name="origen"
                            label="Origen"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Por favor ingrese el prestamista!' 
                                },
                            ]}
                        >
                            <Input style={{ width: '100%' }} />
                        </Form.Item>
                        <Form.Item
                            name="descripcion"
                            label="Descripcion"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Por favor ingrese Descripcion!' 
                                },
                            ]}
                        >
                            <Input style={{ width: '100%' }} />
                        </Form.Item>
                     
                        <Form.Item 
                         wrapperCol={{ 
                            xs: {
                                span: 24,
                                offset: 0,
                            },
                            sm: {
                                span: 24,
                                offset: 5,
                            },
                         }}
                         >
                            <Button 
                            type="primary" 
                            htmlType="submit" block>
                                Registrar
                            </Button>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </div >
    );
};
export default RegisterLoan;