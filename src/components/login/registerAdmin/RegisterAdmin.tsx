import React from 'react';
import {
    Form,
    Input,
    Select,
    Checkbox,
    Button,
    message,
    Row,
    Typography,
    Col,
} from 'antd';
import './RegisterAdmin.css';
import { UserRegisterDto } from '../../../services/users/user.register.dto';
import { LoginService } from '../../../services/login/login.service';

const { Option } = Select;
// const AutoCompleteOption = AutoComplete.Option;
const { Title } = Typography;
const loginService = new LoginService();

const RegisterAdmin = (props: { history: string[]; }) => {
    const [form] = Form.useForm();

    const onFinish = async (values: any) => {
        message.loading('Iniciando Sesión..', 0.5);
        console.log('Received values of form: ', values);
        try {
            const userAdmin = new UserRegisterDto(
                values.password, 
                values.email, 
                values.firstName, 
                values.lastName, 
                values.dni, 
                values.localidad, 
                values.pais, 
                values.provincia,
             
                values.phone,
                values.gender,
                values.calle,
                values.barrio,
                values.numero,
                ''
                )
            console.log('received values user admin dto', userAdmin);
            const res = await loginService.createUserAdmin(userAdmin);
            console.log(res);
            message.success('Su registro fue exitoso..', 0.5);
            props.history.push('/login');
        } catch (error) {
            message.error('Error de registro: ' + error, 2.5);
        }
    };

    return (
        <div className={'root'}>
            <Row justify='space-around' >
                <Title level={2}> Formulario de Registro de Prestamos</Title>
            </Row>
            <Row justify='space-around'>
                <Col className={'form-col'}>
                    <Form
                        labelAlign='left'
                        labelCol={{
                            xs: { span: 24 },
                            sm: { span: 5 }
                        }}
                        form={form}
                        name="register"
                        onFinish={onFinish}
                        scrollToFirstError
                        size={'large'}
                    >
                        <Form.Item
                            name="firstName"
                            label="Nombre"
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su nombre!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            name="lastName"
                            label="Apellido"
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su nombre!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>


                        <Form.Item
                            name="dni"
                            label="DNI"
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su dni!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>


                        <Form.Item
                            name="localidad"
                            label="Localidad"
                            rules={[

                                {
                                    required: true,
                                    message: 'Por favor ingrese su localidad!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            name="email"
                            label="E-mail"
                            rules={[
                                {
                                    type: 'email',
                                    message: 'No es un e-mail!',
                                },
                                {
                                    required: true,
                                    message: 'Por favor ingrese su e-mail!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            name="gender"
                            label="Genero"
                            rules={[
                                {
                                    required: true,
                                    message: 'Por favor seleccione una opción'
                                }
                            ]}>
                            <Select
                                placeholder="Seleccione una opción..."
                                allowClear
                            >
                                <Option value="Masculino">Masculino</Option>
                                <Option value="Femenino">Femenino</Option>
                                <Option value="Otro">Otro</Option>
                            </Select>
                        </Form.Item>

                     

                        <Form.Item
                            name="password"
                            label="Password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Por favor ingrese su password!',
                                },
                            ]}
                            hasFeedback
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item
                            name="phone"
                            label="Télefono"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Por favor ingrese numero de télefono!' 
                                },
                            ]}
                        >
                            <Input style={{ width: '100%' }} />
                        </Form.Item>


                        <Form.Item
                            name="pais"
                            label="Pais"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Por favor ingrese numero de télefono!' 
                                },
                            ]}
                        >
                            <Input style={{ width: '100%' }} />
                        </Form.Item>


                        <Form.Item
                            name="provincia"
                            label="Provincia"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Por favor ingrese numero de télefono!' 
                                },
                            ]}
                        >
                            <Input style={{ width: '100%' }} />
                        </Form.Item>

                        <Form.Item
                            wrapperCol={{ 
                                xs: {
                                    span: 24,
                                    offset: 0,
                                },
                                sm: {
                                    span: 24,
                                    offset: 5,
                                },
                             }}
                            name="agreement"
                            valuePropName="checked"
                            rules={[
                                { validator: (_, value) => value ? Promise.resolve() : Promise.reject('Por favoro acepte los terminos y condiciones') },
                            ]}
                            
                        >
                            <Checkbox>
                                Acepto <a href="/">terminos y condiciones</a>
                            </Checkbox>
                        </Form.Item>
                        <Form.Item 
                         wrapperCol={{ 
                            xs: {
                                span: 24,
                                offset: 0,
                            },
                            sm: {
                                span: 24,
                                offset: 5,
                            },
                         }}
                         >
                            <Button 
                            type="primary" 
                            htmlType="submit" block>
                                Registrar
                            </Button>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </div >
    );
};
export default RegisterAdmin;