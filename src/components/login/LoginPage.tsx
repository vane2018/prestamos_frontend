import React, { useEffect, useContext, useState } from "react";
import jwt_decode from "jwt-decode";
import {
    Button,
    Card,
    Checkbox,
    Form,
    Input,
    Row,
    message
} from 'antd';
import {
    UserOutlined,
    LockOutlined
} from '@ant-design/icons';
import LogoPrestamo from '../../resources/images/logoprestamo.png';
import { LoginService } from '../../services/login/login.service';
import AuthGlobal from '../../auth/store/AuthGlobal';
import { LoginUserDto } from '../../services/login/login.dto';
import { setCurrentUser } from '../../auth/actions/auth.action';
import './Login.css';
import { UserService } from "../../services/users/user.service";
import { IToken } from "../../services/login/token.interface";
import { IRole } from "../../services/users/user.interface";
import 'ant-design-pro/dist/ant-design-pro.css';


const loginService = new LoginService();
const userService = new UserService();

const LoginPage = (props: { history: string[]; }) => {
    const context = useContext(AuthGlobal);
    const [showChild, setShowChild] = useState(false);

    useEffect(() => {
        if (context.stateUser.isAuthenticated === true) {
            props.history.push("/");
        }
        setShowChild(true);
    }, [context.stateUser.isAuthenticated, props.history]);

    const onFinish = async (values: any) => {
        const loginUser = new LoginUserDto(
            values.username,
            values.username,
            values.password,
        )
        try {
            message.loading('Iniciando Sesión..', 0.5);
            const res = await loginService.loginUser(loginUser);
            console.log('Token', res.accessToken);
            const decoded: IToken = jwt_decode(res.accessToken);

            const resUser = await userService.findOneByIdParam(decoded.id);

            resUser.rols.map((rol: IRole) => {
                if (rol.id === 3) {
                    localStorage.setItem("token", res.accessToken);
                    localStorage.setItem("rol", rol.name);
                    context.dispatch(setCurrentUser(decoded));
                    message.success('Sesión Exitosa..', 0.5);
                    return '';

                } else {
                    if (rol.id === 4) {
                        localStorage.setItem("token", res.accessToken);
                        localStorage.setItem("rol", rol.name);
                        context.dispatch(setCurrentUser(decoded));
                        message.success('Sesión Exitosa..', 0.5);
                        return '';
    
                    } else {
                        message.error('Ud. no tiene Rol Administrador', 2);
                        return '';
                    }
                }
            })
        } catch (error) {
            message.error('Email/Nick ó Password Incorrecto', 1.5);

        }
    };

    if (!showChild) {
        return null;
    } else {
        return (
            <Row align='middle' justify='center'>
                <Card
                    style={{ width: 400, marginTop: 50 }}
                >
                    <Row justify='center' style={{ marginBottom: 50, }}>
                        <img src={LogoPrestamo} alt='img/Logo' />
                    </Row>
                    <Form
                        name="normal_login"
                        className="login-form"
                        initialValues={{ remember: true, size: 'large' }}
                        onFinish={onFinish}
                        size='large'

                    >
                        <Form.Item
                            name="username"
                        
                            rules={[{
                                required: true,
                                message: 'Por favor ingrese su Email ó Nick!'
                            }]}
                        >
                            <Input
                                prefix={<UserOutlined className="site-form-item-icon" />}
                                placeholder="Email/Nick"
                            />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            rules={[{
                                required: true,
                                message: 'Porfavor ingrese su clave!'
                            }]}
                        >
                            <Input
                                prefix={<LockOutlined className="site-form-item-icon" />}
                                type="password"
                                placeholder="Clave"
                            />
                        </Form.Item>
                        <Form.Item>
                            <Form.Item
                                name="remember"
                                valuePropName="checked"
                                noStyle
                            >
                                <Checkbox>Recuerdame</Checkbox>
                            </Form.Item>

                            <a className="login-form-forgot" href="/">
                                Olvide mi clave
                            </a>
                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="login-form-button">
                                Iniciar
                            </Button>
                        </Form.Item>
                        <Form.Item>
                            ó <a href="/register-form">Registrarme!</a>
                        </Form.Item>
                    </Form>
                </Card>
            </Row>
        );
    };
};
export default LoginPage;