import React, { useContext, useState, useEffect } from "react";
import jwt_decode from "jwt-decode";
import {
    BrowserRouter as Router,
    Route,
    Switch,
} from 'react-router-dom';
import { Layout, Avatar, Dropdown, Menu, Row, Space } from 'antd';
import { DownOutlined, PoweroffOutlined, UpOutlined, IdcardOutlined } from '@ant-design/icons';
import SiderNav from '../sider/siderNav';

// importación de las paginas
import HomePage from '../home/MenuPage';
import HomePageAdmin from '../home/MenuPageAdmin';
import AplicationPage from '../aplication/AplicationPage';
import UserPage from '../user/UserPage';
import ProductPage from '../product/ProductPage';
import AnalysisPage from '../dashboard/analysis/AnalysisPage';
import CommercePage from '../commerce/CommercePage';
import InConstructionPage from "../inConstructionPage/InConstructionPage";
import MercadopagoPayment from '../mercadopagopaymant/MercadopagoPayment';

//importación de componentes para authentificación
import AuthGlobal from '../../auth/store/AuthGlobal';
import { logoutUser } from '../../auth/actions/auth.action';

//importacion de Servicios, Interface de datos, imagenes, iconos y css
import { UserService } from '../../services/users/user.service';
import { IToken } from "../../services/login/token.interface";
import { IUser } from "../../services/users/user.interface";
import Profile from '../../resources/images/profile.svg';
import 'ant-design-pro/dist/ant-design-pro.css';
import './Layout.css';
import CommissionPage from "../commission/CommissionPage";
import Register from "../register/Register";
import RegisterAdmin from "../login/registerAdmin/RegisterAdmin";
import RegisterLoan from "../registerLoan/RegisterLoan";
import UserDesc from "../user/UserDesc";
import LoanPage from "../loan/LoanPage";
import RegisterEdit from "../register/RegisterEdit";




const userService = new UserService();
const { Header, Sider, Content, Footer } = Layout;


const LayoutPage = (props: { history: string[]; }) => {

    const context = useContext(AuthGlobal);
    const [showChild, setShowChild] = useState(false);
    const [user, setUser] = useState<IUser>();
    const [collapse, setCollapse] = useState(false);
    const [upDownArrow, setupDownArrow] = useState(false);
    const [rolUser, setRolUser] = useState('');

    //función de autentificación de usuarios
    const loadUser = async () => {
        const token = localStorage.getItem("token");
        const userRol = localStorage.getItem('rol');
        if (token && userRol) {
            try {
                const decoded: IToken = jwt_decode(token);
                const res = await userService.findOneByIdParam(decoded.id);
                setUser(res);
                setRolUser(userRol);

            } catch (error) {
                console.log('Error:', error)
            }
        } 

    }

    //función de cerrar sesión
    const logout = () => {
        logoutUser(context.dispatch)
    }

    //funcion que abre y cierrara menu
    const handleClickMenu = () => {
        setupDownArrow(!upDownArrow)

    }

    useEffect(() => {
        window.innerWidth <= 760 ? setCollapse(true) : setCollapse(false);

        if (
            context.stateUser.isAuthenticated === false ||
            context.stateUser.isAuthenticated === null
        ) {
            props.history.push("/login");
        }
        setShowChild(true);
        loadUser();

    }, [context.stateUser.isAuthenticated, props.history]);

    if (!showChild) {
        return null;
    } else {
        return (
            <Router>
                <Layout>
                    <Sider
                        breakpoint="lg"
                        collapsedWidth="0"
                        onBreakpoint={broken => {
                            console.log(broken, collapse);
                        }}
                        onCollapse={(collapsed, type) => {
                            console.log(collapsed, type);

                        }}
                        zeroWidthTriggerStyle={{
                            backgroundColor: 'rgb(37, 58, 104)',
                            marginTop: -52,
                            marginRight: -20,
                            fontSize: 25,
                        }}
                        width={230}
                    >
                        <SiderNav />
                    </Sider>
                    <Layout className='site-layout'>
                        <Header className='site-layout-header-background' style={{ padding: 0 }}>
                            <Row justify='end' align='middle' style={{ paddingLeft: 50, paddingRight: 50 }}>
                                <Space>
                                    {(user?.profilePic === '') ?
                                        <Avatar
                                            size="small"
                                            src={Profile}
                                            style={{ backgroundColor: 'white' }}
                                        /> :
                                        <Avatar
                                            size="small"
                                            src={user?.profilePic}
                                        />
                                    }

                                    <Dropdown
                                        overlay={
                                            <Menu
                                                style={{

                                                    width: 150,
                                                    top: 20
                                                }}
                                            >
                                                <Menu.Item >
                                                    <IdcardOutlined />
                                                        Cuenta
                                                </Menu.Item>
                                                <Menu.Item onClick={logout}>
                                                    <PoweroffOutlined />
                                                    Cerrar sesión
                                                </Menu.Item>
                                            </Menu>

                                        }
                                        trigger={['click']}
                                        placement='bottomCenter'
                                    >
                                        <a href='/#' className="ant-dropdown-link" onClick={handleClickMenu}>
                                            {user?.nickName + ' '}
                                            {upDownArrow === false ? <DownOutlined /> : <UpOutlined style={{ fontSize: 10 }} />}
                                        </a>
                                    </Dropdown>
                                </Space>
                            </Row>
                        </Header>
                        <Content
                            className='site-layout-content-background'
                        >
                            <Switch>
                                <Route exact path="/">
                                    {
                                        (rolUser === 'super') ?
                                        <HomePage {...props} user={user} />:
                                        <HomePageAdmin {...props} user={user} />
                                    }                                    
                                </Route>
                                <Route exact path="/dashboard/analysis" component={AnalysisPage} />
                                <Route exact path="/user" component={UserPage} />
                             {/*    <Route exact path="/commerce" component={CommercePage} /> */}
                                <Route exact path="/product" component={ProductPage} />
                                <Route exact path="/app" component={UserPage} />
                               
                                <Route exact path="/register" component={Register} />
                                 
                                <Route exact path="/loan" component={RegisterLoan} />
                                <Route exact path="/loans" component={LoanPage} />
                                <Route exact path="/userdesc/:id" component={UserDesc} />
                                <Route exact path="/registeredit/:Id" component={RegisterEdit} />
                                <Route exact path="/incontruction-page" component={InConstructionPage} />
                                {/* <Route exact path="/commission-inf" component={CommissionPage} /> */}
                            </Switch>
                        </Content>
                        <Footer
                            className='site-layout-footer'
                        >
                            Prestamos Perico ©2022 Todos los derechos reservados
                    </Footer>
                    </Layout>
                </Layout>
            </Router>
        );
    };
}
export default LayoutPage;