import React from "react";
import { Layout, Row } from 'antd';
//Componentes
import MercadoPagoPage from '../mercadopago/MercadoPagoPage';

// CSS
import 'ant-design-pro/dist/ant-design-pro.css';
import './Layout.css';

//Imagenes y Logos
import logoComprartirWhite from '../../resources/images/logoprestamo.png';


const { Header, Content } = Layout;


const LayoutMercadoPago = () => {

    return (

        <Layout className='site-layout'>
            <Header className='site-layout-header-mercadopago-background' style={{ padding: 0 }}>
                <Row justify='center'>
                    <div
                        style={{
                            display: 'flex',
                            height: "40px",
                            margin: "10px",
                            alignItems: 'center'
                        }
                        }
                    >
                        <img src={logoComprartirWhite} alt={'img/svg'}></img>
                    </div>
                </Row>
            </Header>
            <Content
            >
                <MercadoPagoPage/>
            </Content>
        </Layout>
    );
};

export default LayoutMercadoPago;